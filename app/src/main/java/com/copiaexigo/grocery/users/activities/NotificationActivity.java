package com.copiaexigo.grocery.users.activities;

import android.content.Context;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.adapter.NotificationAdapter;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.models.Notify;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class NotificationActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.ivback)
    ImageView ivback;

    @BindView(R.id.notification_rv)
    RecyclerView notificationRv;
    Context context;

    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    final List<Notify> notificationList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        ButterKnife.bind(this);
        context = NotificationActivity.this;
        //Toolbar
        setSupportActionBar(toolbar);
//        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
//        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        toolbar.setContentInsetsAbsolute(toolbar.getContentInsetLeft(), 0);


        ivback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

//        final ArrayList<NotificationItem> notificationList = new ArrayList<>();
//        notificationList.add(new NotificationItem("10% offer for veg orders", "Sep 08,2017", "Use Code AD123"));
//        notificationList.add(new NotificationItem("5% offer for Non-veg orders", "Sep 15,2017", "Use Code NV124"));


        getNotification();

        //Offer Restaurant Adapter


    }

    private void getNotification() {

        Call<List<Notify>> call = apiInterface.getNotify();

        call.enqueue(new Callback<List<Notify>>() {
            @Override
            public void onResponse(Call<List<Notify>> call, Response<List<Notify>> response) {

                notificationList.addAll(response.body());
                notificationRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                notificationRv.setItemAnimator(new DefaultItemAnimator());
                notificationRv.setHasFixedSize(true);
                NotificationAdapter orderItemListAdapter = new NotificationAdapter(notificationList, context);
                notificationRv.setAdapter(orderItemListAdapter);
            }

            @Override
            public void onFailure(Call<List<Notify>> call, Throwable t) {

                Log.e("errors:",t.toString());

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_nothing, R.anim.slide_out_right);
    }
    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/
}
