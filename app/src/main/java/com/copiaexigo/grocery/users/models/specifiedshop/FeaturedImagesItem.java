package com.copiaexigo.grocery.users.models.specifiedshop;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FeaturedImagesItem implements Serializable {

	@SerializedName("position")
	private Integer position;

	@SerializedName("url")
	private String url;

	public void setPosition(Integer position){
		this.position = position;
	}

	public Integer getPosition(){
		return position;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}
}