package com.copiaexigo.grocery.users.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.copiaexigo.grocery.users.CountryPicker.Country;
import com.copiaexigo.grocery.users.CountryPicker.CountryPicker;
import com.copiaexigo.grocery.users.CountryPicker.CountryPickerListener;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.CustomDialog;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.ForgotPassword;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.forget_btn)
    Button forgetBtn;

    @BindView(R.id.countryNumber)
    TextView countryNumber;
    @BindView(R.id.countryImage)
    ImageView countryImage;
    CustomDialog customDialog;
    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    boolean isSignUp = true;

    String country_code = "+91";
    private CountryPicker mCountryPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ButterKnife.bind(this);
        Bundle bundle =  getIntent().getExtras();
        if (bundle != null) {
            isSignUp = bundle.getBoolean("signup", true);
        }
        setSupportActionBar(toolbar);

        customDialog = new CustomDialog(this);

        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        forgetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(email.getText()!=null && email.getText().toString().length()>=9){
                  //  getOtpVerification(country_code +email.getText().toString());
                    getOtpVerification(email.getText().toString());
                }else Toast.makeText(ForgotPasswordActivity.this, "Enter valid phone number", Toast.LENGTH_SHORT).show();

              /*  Toast.makeText(ForgotPasswordActivity.this, "Otp send your mail", Toast.LENGTH_SHORT).show();
                else Toast.makeText(ForgotPasswordActivity.this, "Enter valid email", Toast.LENGTH_SHORT).show();*/
            }
        });

        mCountryPicker = CountryPicker.newInstance("Select Country");
        // You can limit the displayed countries
        List<Country> countryList = Country.getAllCountries();
        Collections.sort(countryList, new Comparator<Country>() {
            @Override
            public int compare(Country s1, Country s2) {
                return s1.getName().compareToIgnoreCase(s2.getName());
            }
        });
        mCountryPicker.setCountriesList(countryList);
        setListener();
        final boolean once = false;

    }

    public void getOtpVerification(String map) {
        customDialog.show();
        Call<ForgotPassword> call = apiInterface.forgotPassword(map);
        call.enqueue(new Callback<ForgotPassword>() {
            @Override
            public void onResponse(@NonNull Call<ForgotPassword> call, @NonNull Response<ForgotPassword> response) {
                customDialog.dismiss();
                if (response.isSuccessful()) {
                    try{
                        Toast.makeText(ForgotPasswordActivity.this, "" + response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        GlobalData.otpValue = Integer.parseInt(response.body().getUser().getOtp());
                        GlobalData.profileModel =response.body().getUser();
                        startActivity(new Intent(ForgotPasswordActivity.this, OtpActivity.class) .putExtra("signup", isSignUp));
                        finish();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                } else {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        JSONArray jsonArray=new JSONArray(jObjError.optString("phone"));
                        Toast.makeText(ForgotPasswordActivity.this, jsonArray.get(0).toString(), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ForgotPasswordActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ForgotPassword> call, @NonNull Throwable t) {
                customDialog.dismiss();
            }
        });

    }

    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_nothing, R.anim.slide_out_right);
    }
    //check Valid Mail address
    public final static boolean isValidEmail(String strText) {
        return strText != null && android.util.Patterns.EMAIL_ADDRESS.matcher(strText).matches();
    }

    private void setListener() {
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                                        int flagDrawableResID) {
                countryNumber.setText(dialCode);
                country_code = dialCode;
                countryImage.setImageResource(flagDrawableResID);
                mCountryPicker.dismiss();
            }
        });

        countryImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountryPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        countryNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCountryPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
            }
        });

        getUserCountryInfo();
    }

    private void getUserCountryInfo() {
        Locale current = getResources().getConfiguration().locale;
        Country country = Country.getCountryFromSIM(ForgotPasswordActivity.this);
        if (country != null) {
            countryImage.setImageResource(country.getFlag());
            countryNumber.setText(country.getDialCode());
            country_code = country.getDialCode();
        } else {
            Country us = new Country("US", "United States", "+1", R.drawable.flag_us);
            countryImage.setImageResource(us.getFlag());
            countryNumber.setText(us.getDialCode());
            country_code = us.getDialCode();
        }
    }
}
