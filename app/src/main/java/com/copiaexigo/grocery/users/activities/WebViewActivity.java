package com.copiaexigo.grocery.users.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;

import com.copiaexigo.grocery.users.R;

public class WebViewActivity extends AppCompatActivity {

    WebView webView;
    String url;
    String status = "";
    String id = "";

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        webView = findViewById(R.id.webView);
        webView.setWebViewClient(new MyWebView());
        url = getIntent().getStringExtra("url");
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl(url);
    }

    private class MyWebView extends WebViewClient {

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            super.onReceivedError(view, errorCode, description, failingUrl);
            Log.d("TestTag", "Failure Url " + failingUrl);
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            webView.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.d("TestTag", "URL== after success1 : " + url);
            super.onPageFinished(view, url);

            Uri uri = Uri.parse(url);
            if (uri.getQueryParameter("redirect_status") != null) {
                status = uri.getQueryParameter("redirect_status");
            }

            if (url.contains("https://nightlivery.be/orders")) {
                Uri orderUri = Uri.parse(url);
                id = orderUri.getLastPathSegment();

                assert status != null;
                if (status.equals("succeeded") && !id.isEmpty()) {
                    Intent intent = new Intent();
                    intent.putExtra("id", id);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        }
    }
}