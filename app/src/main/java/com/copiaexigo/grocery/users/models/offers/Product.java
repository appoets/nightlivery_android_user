
package com.copiaexigo.grocery.users.models.offers;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Product implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("brand")
    @Expose
    private String brand;
    @SerializedName("nutrition")
    @Expose
    private String nutrition;
    @SerializedName("ingredients")
    @Expose
    private String ingredients;
    @SerializedName("directions")
    @Expose
    private String directions;
    @SerializedName("warnings")
    @Expose
    private String warnings;
    @SerializedName("product_info_img")
    @Expose
    private String productInfoImg;
    @SerializedName("position")
    @Expose
    private Integer position;
    @SerializedName("food_type")
    @Expose
    private String foodType;
    @SerializedName("avalability")
    @Expose
    private Integer avalability;
    @SerializedName("max_quantity")
    @Expose
    private Integer maxQuantity;
    @SerializedName("featured")
    @Expose
    private Integer featured;
    @SerializedName("featured_position")
    @Expose
    private Integer featuredPosition;
    @SerializedName("addon_status")
    @Expose
    private Integer addonStatus;
    @SerializedName("cuisine_id")
    @Expose
    private Integer cuisineId;
    @SerializedName("out_of_stock")
    @Expose
    private String outOfStock;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("prices")
    @Expose
    private Prices prices;

    protected Product(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            shopId = null;
        } else {
            shopId = in.readInt();
        }
        name = in.readString();
        description = in.readString();
        brand = in.readString();
        nutrition = in.readString();
        ingredients = in.readString();
        directions = in.readString();
        warnings = in.readString();
        productInfoImg = in.readString();
        if (in.readByte() == 0) {
            position = null;
        } else {
            position = in.readInt();
        }
        foodType = in.readString();
        if (in.readByte() == 0) {
            avalability = null;
        } else {
            avalability = in.readInt();
        }
        if (in.readByte() == 0) {
            maxQuantity = null;
        } else {
            maxQuantity = in.readInt();
        }
        if (in.readByte() == 0) {
            featured = null;
        } else {
            featured = in.readInt();
        }
        if (in.readByte() == 0) {
            featuredPosition = null;
        } else {
            featuredPosition = in.readInt();
        }
        if (in.readByte() == 0) {
            addonStatus = null;
        } else {
            addonStatus = in.readInt();
        }
        if (in.readByte() == 0) {
            cuisineId = null;
        } else {
            cuisineId = in.readInt();
        }
        outOfStock = in.readString();
        status = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getNutrition() {
        return nutrition;
    }

    public void setNutrition(String nutrition) {
        this.nutrition = nutrition;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }

    public String getWarnings() {
        return warnings;
    }

    public void setWarnings(String warnings) {
        this.warnings = warnings;
    }

    public String getProductInfoImg() {
        return productInfoImg;
    }

    public void setProductInfoImg(String productInfoImg) {
        this.productInfoImg = productInfoImg;
    }

    public Integer getCuisineId() {
        return cuisineId;
    }

    public void setCuisineId(Integer cuisineId) {
        this.cuisineId = cuisineId;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public Integer getAvalability() {
        return avalability;
    }

    public void setAvalability(Integer avalability) {
        this.avalability = avalability;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    public Integer getFeaturedPosition() {
        return featuredPosition;
    }

    public void setFeaturedPosition(Integer featuredPosition) {
        this.featuredPosition = featuredPosition;
    }

    public Integer getAddonStatus() {
        return addonStatus;
    }

    public void setAddonStatus(Integer addonStatus) {
        this.addonStatus = addonStatus;
    }


    public String getOutOfStock() {
        return outOfStock;
    }

    public void setOutOfStock(String outOfStock) {
        this.outOfStock = outOfStock;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Prices getPrices() {
        return prices;
    }

    public void setPrices(Prices prices) {
        this.prices = prices;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        if (shopId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(shopId);
        }
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(brand);
        dest.writeString(nutrition);
        dest.writeString(ingredients);
        dest.writeString(directions);
        dest.writeString(warnings);
        dest.writeString(productInfoImg);
        if (position == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(position);
        }
        dest.writeString(foodType);
        if (avalability == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(avalability);
        }
        if (maxQuantity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(maxQuantity);
        }
        if (featured == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(featured);
        }
        if (featuredPosition == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(featuredPosition);
        }
        if (addonStatus == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(addonStatus);
        }
        if (cuisineId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(cuisineId);
        }
        dest.writeString(outOfStock);
        dest.writeString(status);
    }

}
