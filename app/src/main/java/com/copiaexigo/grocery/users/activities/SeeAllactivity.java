package com.copiaexigo.grocery.users.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.adapter.BannerAdapter;
import com.copiaexigo.grocery.users.adapter.HomeCategoryAdapter;
import com.copiaexigo.grocery.users.adapter.ProductAdapter;
import com.copiaexigo.grocery.users.adapter.RestaurantsAdapter;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.CategoriesItem;
import com.copiaexigo.grocery.users.models.HomeCategoryResponse;
import com.copiaexigo.grocery.users.models.Shop;
import com.copiaexigo.grocery.users.models.offers.OffersModel;
import com.copiaexigo.grocery.users.models.offers.Product;
import com.copiaexigo.grocery.users.models.specifiedshop.ProductItem;
import com.copiaexigo.grocery.users.models.specifiedshop.SpecifiedShopResponse;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.SkeletonScreen;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copiaexigo.grocery.users.helper.GlobalData.latitude;
import static com.copiaexigo.grocery.users.helper.GlobalData.longitude;

public class SeeAllactivity extends AppCompatActivity  {

    @BindView(R.id.tv_allItemTitle)
    TextView tvTitle;
    @BindView(R.id.rvAllItems)
    RecyclerView rvAllItems;

    @BindView(R.id.back)
    ImageView back;

    BannerAdapter bannerAdapter;
    List<Product> bannerList = new ArrayList<>();

    ProductAdapter adapterProduct;

    //shops see All
    private List<ProductItem> featuredProductItems = new ArrayList<>();
    private List<ProductItem> salesProductItems = new ArrayList<>();



    private SkeletonScreen skeletonScreen2,skeletonText2;
    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);

    //for All Categories
    HomeCategoryAdapter adapterDiscover;
    final List<CategoriesItem> discoverList = new ArrayList<>();

    //for All Shops
    RestaurantsAdapter adapterRestaurant;
    List<Shop> restaurantList = new ArrayList<>();
    String seeAllCallfrom;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_allactivity);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("CALL_FROM")) {

            if (getIntent().getStringExtra("CALL_FROM")!=null){

                seeAllCallfrom = getIntent().getStringExtra("CALL_FROM");
            }

        }

        skeletonScreen2 = Skeleton.bind(rvAllItems)
                .adapter(bannerAdapter)
                .load(R.layout.skeleton_impressive_list_item_verticle)
                .count(3)
                .show();
        skeletonText2 = Skeleton.bind(tvTitle)
                .load(R.layout.skeleton_label)
                .show();

        skeletonScreen2.show();
        skeletonText2.show();

        if (seeAllCallfrom.equals("CATEGORIES")){

            tvTitle.setText(this.getString(R.string.categories));
            getCategoryList();

        }else if (seeAllCallfrom.equals("DEALS")){

            tvTitle.setText(this.getString(R.string.deals_amp_offers));
            getOffers();

        }else if (seeAllCallfrom.equals("SHOPS")){

            tvTitle.setText(this.getString(R.string.shops_near_you));
            getRestaurant();

        }else if (seeAllCallfrom.equals("SALES")){

            tvTitle.setText(this.getString(R.string.sales));
            getRestaurantByShopId();

        }else if (seeAllCallfrom.equals("FEATURED")){

            tvTitle.setText(this.getString(R.string.featured_products));
            getRestaurantByShopId();

        }else{
            onBackPressed();
        }


        setUpRecyclerView();

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private void setUpRecyclerView() {


        rvAllItems.setHasFixedSize(true);
        rvAllItems.setItemViewCacheSize(20);
        rvAllItems.setDrawingCacheEnabled(true);
        rvAllItems.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvAllItems.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvAllItems.setItemAnimator(new DefaultItemAnimator());


    }

    public void getOffers() {

//        skeletonScreen2.hide();
        skeletonText2.hide();


        Call<OffersModel> call = apiInterface.getHomeOfferList();
        call.enqueue(new Callback<OffersModel>() {
            @Override
            public void onResponse(Call<OffersModel> call, Response<OffersModel> response) {
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(SeeAllactivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(SeeAllactivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    //Check Banner list
                    assert response.body() != null;
                    if (response.body().getProducts().size() == 0 ) {
                        tvTitle.setVisibility(View.GONE);
                    } else {
                        tvTitle.setVisibility(View.VISIBLE);
                    }
                    bannerList.clear();
                    bannerList.addAll(response.body().getProducts());

                    bannerAdapter = new BannerAdapter(bannerList, SeeAllactivity.this, SeeAllactivity.this,R.layout.seeall_list_item);
                    rvAllItems.setAdapter(bannerAdapter);
                }
            }

            @Override
            public void onFailure(Call<OffersModel> call, Throwable t) {
                Log.d("dealsFail",t.getLocalizedMessage());
            }
        });


    }

    public void getCategoryList() {

        skeletonText2.hide();

        Call<HomeCategoryResponse> call = apiInterface.getHomeCategoryList();
        call.enqueue(new Callback<HomeCategoryResponse>() {
            @Override
            public void onResponse(Call<HomeCategoryResponse> call, Response<HomeCategoryResponse> response) {
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(SeeAllactivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(SeeAllactivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    //Check Banner list

                    if (response.body().getCategories().size() == 0 ) {
                        tvTitle.setVisibility(View.GONE);
                    } else {
                        tvTitle.setVisibility(View.VISIBLE);
                    }

                    discoverList.clear();
                    discoverList.addAll(response.body().getCategories());

                    adapterDiscover = new HomeCategoryAdapter(discoverList, SeeAllactivity.this,R.layout.home_category_all_list_item);
                    rvAllItems.setAdapter(adapterDiscover);

                }
            }

            @Override
            public void onFailure(Call<HomeCategoryResponse> call, Throwable t) {

            }
        });


    }


    private void getRestaurant() {

        skeletonText2.hide();
        restaurantList.clear();
        restaurantList.addAll(GlobalData.shopList);
        adapterRestaurant = new RestaurantsAdapter(restaurantList, SeeAllactivity.this,  SeeAllactivity.this,R.layout.restaurant_all_list_item);
        rvAllItems.setAdapter(adapterRestaurant);

    }

    private void getRestaurantByShopId() {

        skeletonText2.hide();

        HashMap<String, String> map = new HashMap<>();
        map.put("latitude", String.valueOf(latitude));
        map.put("longitude", String.valueOf(longitude));

/*        map.put("latitude", String.valueOf(43.8542707));
        map.put("longitude", String.valueOf(-79.4960454))*/
        ;

        Call<SpecifiedShopResponse> call = apiInterface.getShopById(GlobalData.selectedShop.getId(), map);
        call.enqueue(new Callback<SpecifiedShopResponse>() {
            @Override
            public void onResponse(Call<SpecifiedShopResponse> call, Response<SpecifiedShopResponse> response) {
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(SeeAllactivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(SeeAllactivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    //Check Restaurant list

                    if (response.body().getCategories().size() == 0 ) {
                        tvTitle.setVisibility(View.GONE);
                    } else {
                        tvTitle.setVisibility(View.VISIBLE);
                    }

                    if (seeAllCallfrom.equals("FEATURED")) {
                        featuredProductItems.clear();
                        featuredProductItems.addAll(response.body().getFeaturedProducts());
//                    adapterProduct.notifyDataSetChanged();
                        adapterProduct = new ProductAdapter(featuredProductItems, SeeAllactivity.this,R.layout.accompainment_all_list_item);

                    }


                    if (response.body().getCategories().size() == 0 ) {
                        tvTitle.setVisibility(View.GONE);
                    } else {
                        tvTitle.setVisibility(View.VISIBLE);
                    }

                    if (seeAllCallfrom.equals("SALES")) {
                        salesProductItems.clear();
                        salesProductItems.addAll(response.body().getSaleProducts());
//                    adapterSales.notifyDataSetChanged();
                        adapterProduct = new ProductAdapter(salesProductItems, SeeAllactivity.this,R.layout.accompainment_all_list_item);
                    }


                }

                GridLayoutManager manager = new GridLayoutManager(SeeAllactivity.this, 2, GridLayoutManager.VERTICAL, false);
                rvAllItems.setLayoutManager(manager);
                rvAllItems.setAdapter(adapterProduct);
            }

            @Override
            public void onFailure(Call<SpecifiedShopResponse> call, Throwable t) {
                Toast.makeText(SeeAllactivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();

            }
        });
    }






}