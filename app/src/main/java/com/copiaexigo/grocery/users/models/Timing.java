
package com.copiaexigo.grocery.users.models;

import com.google.gson.annotations.SerializedName;

public class Timing {

    @SerializedName("shop_id")
    private Integer shopId;

    @SerializedName("start_time")
    private String startTime;

    @SerializedName("end_time")
    private String endTime;

    @SerializedName("id")
    private Integer id;

    @SerializedName("day")
    private String day;

    public void setShopId(Integer shopId){
        this.shopId = shopId;
    }

    public Integer getShopId(){
        return shopId;
    }

    public void setStartTime(String startTime){
        this.startTime = startTime;
    }

    public String getStartTime(){
        return startTime;
    }

    public void setEndTime(String endTime){
        this.endTime = endTime;
    }

    public String getEndTime(){
        return endTime;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public Integer getId(){
        return id;
    }

    public void setDay(String day){
        this.day = day;
    }

    public String getDay(){
        return day;
    }
}
