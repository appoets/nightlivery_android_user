package com.copiaexigo.grocery.users.adapter;

import android.app.Activity;
import android.content.Context;

import android.content.Intent;
import android.graphics.Paint;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.copiaexigo.grocery.users.HomeActivity;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.activities.ViewCartActivity;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.AddCart;
import com.copiaexigo.grocery.users.models.offers.Product;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copiaexigo.grocery.users.helper.GlobalData.notificationCount;
import static com.copiaexigo.grocery.users.helper.GlobalData.selectedShopId;

/**
 * Created by santhosh@appoets.com on 22-08-2017.
 */

public class BannerAdapter extends RecyclerView.Adapter<BannerAdapter.MyViewHolder> {
    private List<Product> list;
    private Context context;
    private Activity activity;

    private int itemLayout;

    public BannerAdapter(List<Product> list, Context con, Activity activity,int itemLayout) {
        this.list = list;
        this.context = con;
        this.activity = activity;
        this.itemLayout = itemLayout;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(itemLayout, parent, false);
        return new MyViewHolder(itemView);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Product product = list.get(position);

        Glide.with(context)
                .load(product.getProductInfoImg())
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.trending_now)
                        .error(R.drawable.trending_now))
                .into(holder.bannerImg);

        holder.tvTitle.setText(product.getName());
        holder.tvOldPrice.setPaintFlags(holder.tvOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.tvOldPrice.setText(product.getPrices().getCurrency()+""+product.getPrices().getPrice());
        holder.tvNewPrice.setText(product.getPrices().getCurrency()+""+product.getPrices().getOrignalPrice());
        holder.saveBtn.setText("Save "+""+product.getPrices().getCurrency()+""+product.getPrices().getDiscount());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(context, ""+product.getName(), Toast.LENGTH_SHORT).show();

                HashMap<String, String> map = new HashMap<>();
                map.put("product_id", product.getId().toString());
                map.put("quantity", "1");
                Log.e("AddCart_Text", map.toString());
                holder.addCart(map);

                context.startActivity(new Intent(context, ViewCartActivity.class));
/*                Banner banner = list.get(position);
                context.startActivity(new Intent(context, HotelViewActivity.class));
                GlobalData.selectedShop = banner.getShop();
                activity.overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                Log.d("Hello", "onItemClick position: " + banner.getShop().getName());*/
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView bannerImg;
        TextView tvTitle,tvNewPrice,tvOldPrice , saveBtn;
        public ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);

        public AddCart addCart;
        public double priceAmount = 0;
        public int itemCount = 0;
        public int itemQuantity = 0;

        public MyViewHolder(View view) {
            super(view);
            tvTitle = view.findViewById(R.id.title);
            tvNewPrice = view.findViewById(R.id.new_price);
            tvOldPrice = view.findViewById(R.id.old_price);
            bannerImg = view.findViewById(R.id.banner_img);
            saveBtn = view.findViewById(R.id.save_);
        }

        public void addCart(HashMap<String, String> map) {
            Call<AddCart> call = apiInterface.postAddCart(map);
            call.enqueue(new Callback<AddCart>() {
                @Override
                public void onResponse(Call<AddCart> call, Response<AddCart> response) {

                    if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else if (response.isSuccessful()) {
                        GlobalData.addCartShopId = selectedShopId;
                        addCart = response.body();
                        GlobalData.addCart = response.body();

                        priceAmount = 0;
                        itemQuantity = 0;
                        itemCount = 0;
                        //get Item Count
                        itemCount = addCart.getProductList().size();
                        for (int i = 0; i < itemCount; i++) {
                            //Get Total item Quantity
                            itemQuantity = itemQuantity + addCart.getProductList().get(i).getQuantity();
                            //Get addon price
                            if (addCart.getProductList().get(i).getProduct().getPrices().getPrice() != null)
                                priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity() * addCart.getProductList().get(i).getProduct().getPrices().getPrice());
                        }

                        notificationCount = itemQuantity;
                        HomeActivity.updateNotificationCount(context, notificationCount);
                    }
                }

                @Override
                public void onFailure(Call<AddCart> call, Throwable t) {

                }
            });

        }

    }


}
