package com.copiaexigo.grocery.users.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.models.specifiedshop.ImagesItem;
import com.copiaexigo.grocery.users.utils.Utils;
import java.util.ArrayList;
import java.util.List;

public class ProductImageAdapter extends PagerAdapter {
    private List<ImagesItem> imagesItems = new ArrayList<>();
    private Context context;

    public ProductImageAdapter(List<ImagesItem> imageItems, Context context) {
        this.imagesItems = imageItems;
        this.context = context;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.item_product_image, container, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.img_product);
        ImagesItem item = imagesItems.get(position);

        if (item.getUrl() != null && !item.getUrl().equals(""))
            Utils.setImageViewGlide(context, imageView, item.getUrl());

        container.addView(view);
        return view;
    }

    @Override
    public int getCount() {
        return imagesItems.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
        return view == obj;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
        View view = (View) object;
        container.removeView(view);
    }
}