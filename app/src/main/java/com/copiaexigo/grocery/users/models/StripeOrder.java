package com.copiaexigo.grocery.users.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class StripeOrder {
    @SerializedName("url")
    @Expose
    String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
