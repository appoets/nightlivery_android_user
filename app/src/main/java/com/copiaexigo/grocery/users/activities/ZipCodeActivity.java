package com.copiaexigo.grocery.users.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.copiaexigo.grocery.users.HomeActivity;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.helper.GlobalData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ZipCodeActivity extends AppCompatActivity {

    @BindView(R.id.back_img)
    ImageView backImg;
    @BindView(R.id.etZipCode)
    EditText etZipCode;
    @BindView(R.id.clickNext)
    Button clickNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zip_code);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.back_img, R.id.clickNext})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                onBackPressed();
                break;
            case R.id.clickNext:
                String zipCode = etZipCode.getText().toString();
                if (zipCode.isEmpty()) {
                    Toast.makeText(this, R.string.please_enter_zipocode, Toast.LENGTH_LONG).show();
                } else {
                    GlobalData.zipCode = zipCode;
                    startActivity(new Intent(ZipCodeActivity.this, HomeActivity.class)
                            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finishAffinity();
                }
                break;
        }
    }
}
