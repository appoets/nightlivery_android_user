package com.copiaexigo.grocery.users.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.widget.Toast;

import com.copiaexigo.grocery.users.BuildConfig;
import com.copiaexigo.grocery.users.R;

public class ReferFriendActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refeerr_friend);


        findViewById(R.id.back).setOnClickListener(view -> onBackPressed());

        findViewById(R.id.invite_now).setOnClickListener(view -> shareDetails("Hey I am using " + getString(R.string.app_name) +
                " and invite you to join! https://play.google.com/store/apps/details?id="
                + BuildConfig.APPLICATION_ID +
                "\nHave a Good day!\n"));


    }

    private void shareDetails(String shareRideText) {
        try {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            sendIntent.putExtra(Intent.EXTRA_TEXT, shareRideText);
            sendIntent.setType("text/plain");
            sendIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(Intent.createChooser(sendIntent, getString(R.string.share)));
        } catch (Exception e) {
            Toast.makeText(this, "applications not found!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_nothing, R.anim.slide_out_right);
    }

}
