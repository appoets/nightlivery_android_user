package com.copiaexigo.grocery.users.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.widget.NestedScrollView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.copiaexigo.grocery.users.HeaderView;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.adapter.HotelCatagoeryAdapter;
import com.copiaexigo.grocery.users.adapter.ProductAdapter;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.ConnectionHelper;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.AddCart;
import com.copiaexigo.grocery.users.models.Category;
import com.copiaexigo.grocery.users.models.CategoryPackages;
import com.copiaexigo.grocery.users.models.Favorite;
import com.copiaexigo.grocery.users.models.Product;
import com.copiaexigo.grocery.users.models.Shop;
import com.copiaexigo.grocery.users.models.ShopDetail;
import com.copiaexigo.grocery.users.models.specifiedshop.ProductItem;
import com.copiaexigo.grocery.users.models.specifiedshop.SpecifiedShopResponse;
import com.copiaexigo.grocery.users.utils.Utils;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.ViewSkeletonScreen;
import com.sackcentury.shinebuttonlib.ShineButton;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
//import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static com.copiaexigo.grocery.users.adapter.HotelCatagoeryAdapter.bottomSheetDialogFragment;

public class HotelViewActivity extends AppCompatActivity
        implements AppBarLayout.OnOffsetChangedListener{

    //    @BindView(R.id.scroll_view)
//    NestedScrollView scrollView;
    public static TextView itemText;
    public static TextView viewCartShopName;
    public static TextView viewCart;
    public static RelativeLayout viewCartLayout;
    public static Shop shops;
    public static List<Category> categoryList;
    public static List<Product> featureProductList;
    public static HotelCatagoeryAdapter catagoeryAdapter;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recommended_dishes_rv)
    RecyclerView recommendedDishesRv;
    @BindView(R.id.accompaniment_dishes_rv)
    RecyclerView accompanimentDishesRv;
    @BindView(R.id.weekly_savings_)
    RecyclerView weeklyRecyclerView;
    @BindView(R.id.heart_btn)
    ShineButton heartBtn;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.llFeaturedProduct)
    LinearLayout llFeaturedProduct;
    @BindView(R.id.llSalesProduct)
    LinearLayout llSalesProduct;
    @BindView(R.id.weekly_savings_layout)
    LinearLayout weekly_savings_layout;
    @BindView(R.id.restaurant_title2)
    TextView restaurantTitle2;
    @BindView(R.id.restaurant_subtitle2)
    TextView restaurantSubtitle2;
    @BindView(R.id.restaurant_image)
    ImageView restaurantImage;
    @BindView(R.id.header_view_title)
    TextView headerViewTitle;
    @BindView(R.id.header_view_sub_title)
    TextView headerViewSubTitle;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.app_bar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.scroll)
    NestedScrollView scroll;
    @BindView(R.id.offer)
    TextView offer;
    @BindView(R.id.rating)
    TextView rating;
    @BindView(R.id.sales_txtView)
    TextView salesTxtView;
    @BindView(R.id.delivery_time)
    TextView deliveryTime;
    @BindView(R.id.root_layout)
    CoordinatorLayout rootLayout;
    @BindView(R.id.toolbar_header_view)
    HeaderView toolbarHeaderView;
    @BindView(R.id.float_header_view)
    HeaderView floatHeaderView;
    int restaurantPosition = 0;
    boolean isShopIsChanged = true;
    double priceAmount = 0;
    int itemCount = 0;
    int itemQuantity = 0;
    Animation slide_down, slide_up;
    @BindView(R.id.rv_featured_product_)
    RecyclerView rvFeaturedProduct;
    @BindView(R.id.rv_sales_)
    RecyclerView rvSalesProduct;
    ProductAdapter adapterProduct;
    ProductAdapter adapterSales;
    Context context;
    ConnectionHelper connectionHelper;
    Activity activity;
    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    ViewSkeletonScreen skeleton;
    boolean isFavourite = false;
    private boolean isHideToolbarView = false;
    private Boolean isCategory;
    private int categoryPosition;
    private String categoryTitle;
    private String categoryImage;
    private List<ProductItem> featuredProductItems = new ArrayList<>();
    private List<ProductItem> salesProductItems = new ArrayList<>();

    @BindView(R.id.sales_seeall)
    TextView tvSalesSeeAll;
    @BindView(R.id.see_all_featured)
    TextView tvSeeAllFeatured;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_view);
        ButterKnife.bind(this);
        context = HotelViewActivity.this;
        activity = HotelViewActivity.this;
        connectionHelper = new ConnectionHelper(context);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        isCategory = getIntent().getBooleanExtra("isCategory", false);
        categoryPosition = getIntent().getIntExtra("categoryPosition", 0);
        categoryTitle = getIntent().getStringExtra("categoryTitle");
        categoryImage = getIntent().getStringExtra("categoryImage");

        if (!isCategory) {
            weekly_savings_layout.setVisibility(View.GONE);
            adapterProduct = new ProductAdapter(featuredProductItems, context,R.layout.accompainment_list_item);
            rvFeaturedProduct.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            rvFeaturedProduct.setItemAnimator(new DefaultItemAnimator());
            rvFeaturedProduct.setAdapter(adapterProduct);
        }

        if (isCategory) {
            weekly_savings_layout.setVisibility(View.GONE);
            weeklyRecyclerView.setVisibility(View.GONE);
            llFeaturedProduct.setVisibility(View.GONE);
            salesTxtView.setText(categoryTitle);
            tvSalesSeeAll.setVisibility(View.GONE);
            imageLoading(categoryImage);
        }

        adapterSales = new ProductAdapter(salesProductItems, context,R.layout.accompainment_list_item);
        rvSalesProduct.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
        rvSalesProduct.setItemAnimator(new DefaultItemAnimator());
        rvSalesProduct.setAdapter(adapterSales);

        appBarLayout.addOnOffsetChangedListener(this);
        categoryList = new ArrayList<>();
        shops = GlobalData.selectedShop;

        viewCart = findViewById(R.id.view_cart);
        viewCartLayout = findViewById(R.id.view_cart_layout);

        itemText = findViewById(R.id.item_text);
        viewCartShopName = findViewById(R.id.view_cart_shop_name);

        viewCartLayout.setOnClickListener(v -> {
            startActivity(new Intent(HotelViewActivity.this, ViewCartActivity.class));
            overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
        });
        //Load animation
        slide_down = AnimationUtils.loadAnimation(context,
                R.anim.slide_down);
        slide_up = AnimationUtils.loadAnimation(context,
                R.anim.slide_up);

        if (!isCategory) {
            if (shops != null) {



                isFavourite = getIntent().getBooleanExtra("is_fav", false);

                if (shops.getOfferPercent() == null) {
                    offer.setVisibility(View.GONE);
                } else {
                    offer.setVisibility(View.VISIBLE);
                    offer.setText("Flat " + shops.getOfferPercent().toString() + "% offer on all Orders");
                }
                if (shops.getRating() != null) {
                    Double ratingValue = new BigDecimal(shops.getRating()).setScale(1, RoundingMode.HALF_UP).doubleValue();
                    rating.setText("" + ratingValue);
                } else
                    rating.setText("No Rating");

                deliveryTime.setText(shops.getEstimatedDeliveryTime().toString() + "Mins");

                imageLoading(shops.getAvatar());

                Picasso.get()
                        .load(shops.getAvatar())
                        .into(new Target() {
                            @Override
                            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                assert restaurantImage != null;
                                restaurantImage.setImageBitmap(bitmap);
                                Palette.from(bitmap)
                                        .generate(new Palette.PaletteAsyncListener() {
                                            @Override
                                            public void onGenerated(Palette palette) {
                                                Palette.Swatch textSwatch = palette.getDarkMutedSwatch();
                                                if (textSwatch == null) {
                                                    textSwatch = palette.getMutedSwatch();
                                                    if (textSwatch != null) {
                                                        collapsingToolbar.setContentScrimColor(textSwatch.getRgb());
                                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                            Window window = getWindow();
                                                            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                                            window.setStatusBarColor(textSwatch.getRgb());
                                                        }
                                                        headerViewTitle.setTextColor(textSwatch.getTitleTextColor());
                                                        headerViewSubTitle.setTextColor(textSwatch.getBodyTextColor());
                                                    }
                                                } else {
                                                    collapsingToolbar.setContentScrimColor(textSwatch.getRgb());
                                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                        Window window = getWindow();
                                                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                                        window.setStatusBarColor(textSwatch.getRgb());
                                                    }
                                                    headerViewTitle.setTextColor(textSwatch.getTitleTextColor());
                                                    headerViewSubTitle.setTextColor(textSwatch.getBodyTextColor());
                                                }

                                            }
                                        });

                            }

                            @Override
                            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

                            }

                            @Override
                            public void onPrepareLoad(Drawable placeHolderDrawable) {

                            }
                        });

                //Set title
                collapsingToolbar.setTitle(" ");
                toolbarHeaderView.bindTo(shops.getName(), shops.getDescription());
                floatHeaderView.bindTo(shops.getName(), shops.getDescription());

                //Set Categoery shopList adapter
                catagoeryAdapter = new HotelCatagoeryAdapter(this, activity, categoryList);
                accompanimentDishesRv.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                accompanimentDishesRv.setItemAnimator(new DefaultItemAnimator());
                accompanimentDishesRv.setAdapter(catagoeryAdapter);

                if (heartBtn != null)
                    heartBtn.init(this);
                if (shops.getFavorite() != null || isFavourite) {
                    heartBtn.setChecked(true);
                    heartBtn.setTag(1);
                } else
                    heartBtn.setTag(0);
                heartBtn.setShineDistanceMultiple(1.8f);
                heartBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (heartBtn.getTag().equals(0)) {
                            heartBtn.setTag(1);
                            //heartBtn.setShapeResource(R.raw.heart);
                        } else {
                            heartBtn.setTag(0);
                            //heartBtn.setShapeResource(R.raw.icc_heart);
                        }

                    }
                });
                heartBtn.setOnCheckStateChangeListener(new ShineButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(View view, boolean checked) {
                        Log.e("HrtButton", "click " + checked);
                        if (connectionHelper.isConnectingToInternet()) {
                            if (checked) {
                                if (GlobalData.profileModel != null)
                                    doFavorite(shops.getId());
                                else {
                                    startActivity(new Intent(context, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.anim_nothing);
                                    finish();
                                }
                            } else {
                                deleteFavorite(shops.getId());
                            }
                        } else {
                            Utils.displayMessage(activity, context, getString(R.string.oops_connect_your_internet));
                        }
                    }
                });
                skeleton = Skeleton.bind(rootLayout)
                        .load(R.layout.skeleton_hotel_view)
                        .show();

            } else {
                startActivity(new Intent(context, SplashActivity.class));
                finish();
            }
        }



        tvSalesSeeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SeeAllactivity.class);
                intent.putExtra("CALL_FROM","SALES");
                context.startActivity(intent);
            }
        });

        tvSeeAllFeatured.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, SeeAllactivity.class);
                intent.putExtra("CALL_FROM","FEATURED");
                context.startActivity(intent);
            }
        });

    }

    public void imageLoading(String imgURL) {
        Glide.with(context)
                .load(imgURL)
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.ic_restaurant_place_holder)
                        .error(R.drawable.ic_restaurant_place_holder))
                .into(restaurantImage);
    }

    public void getCategoryData() {
        HashMap<String, String> map = new HashMap<>();
        map.put("user_id", String.valueOf(GlobalData.profileModel.getId()));
        ApiClient.getRetrofit().create(ApiInterface.class)
                .getParticularcCategoryList(categoryPosition,map).enqueue(new Callback<CategoryPackages>() {
            @Override
            public void onResponse(Call<CategoryPackages> call, Response<CategoryPackages> response) {
//                Log.i("onResponse: ",response.body().toString());
                if (response.isSuccessful()) {
                    if (response.body() != null && response.body().getProducts() != null) {
                        salesProductItems.clear();
                        salesProductItems.addAll(response.body().getProducts());
                        adapterSales.notifyDataSetChanged();
                        if (GlobalData.addCart != null && GlobalData.addCart.getProductList().size() != 0) {
                            setViewcartBottomLayout(GlobalData.addCart);
                        } else {
                            viewCartLayout.setVisibility(View.GONE);
                        }
                    }
                } else
                    Toast.makeText(context, "No list in this category", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<CategoryPackages> call, Throwable t) {

            }
        });
    }

    private void getRestaurantByShopId() {
        HashMap<String, String> map = new HashMap<>();
       /* map.put("latitude", String.valueOf(latitude));
        map.put("longitude", String.valueOf(longitude));*/
        map.put("user_id", String.valueOf(GlobalData.profileModel.getId()));
/*        map.put("latitude", String.valueOf(43.8542707));
        map.put("longitude", String.valueOf(-79.4960454))*/
        ;

        try{
            Call<SpecifiedShopResponse> call = apiInterface.getShopById(GlobalData.selectedShop.getId(), map);
            call.enqueue(new Callback<SpecifiedShopResponse>() {
                @Override
                public void onResponse(Call<SpecifiedShopResponse> call, Response<SpecifiedShopResponse> response) {
                    if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                        try {
                            JSONObject jObjError = new JSONObject(response.errorBody().string());
                            Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    } else if (response.isSuccessful()) {
                        //Check Restaurant list

                        if (response.body().getFeaturedProducts().size() == 0) {
                            llFeaturedProduct.setVisibility(View.GONE);
                        } else {
                            llFeaturedProduct.setVisibility(View.VISIBLE);
                        }

                        featuredProductItems.clear();
                        featuredProductItems.addAll(response.body().getFeaturedProducts());
                        adapterProduct.notifyDataSetChanged();

                        if (response.body().getSaleProducts().size() == 0) {
                            llSalesProduct.setVisibility(View.GONE);
                        } else {
                            llSalesProduct.setVisibility(View.VISIBLE);
                        }

                        salesProductItems.clear();
                        salesProductItems.addAll(response.body().getSaleProducts());
                        adapterSales.notifyDataSetChanged();

                    }
                }

                @Override
                public void onFailure(Call<SpecifiedShopResponse> call, Throwable t) {
                    Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show();

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void deleteFavorite(Integer id) {
        Call<Favorite> call = apiInterface.deleteFavorite(id);
        call.enqueue(new Callback<Favorite>() {
            @Override
            public void onResponse(@NonNull Call<Favorite> call, @NonNull Response<Favorite> response) {
                Favorite favorite = response.body();
                Toast.makeText(context, "" + favorite.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(@NonNull Call<Favorite> call, @NonNull Throwable t) {

            }
        });

    }

    private void doFavorite(Integer id) {
        Call<Favorite> call = apiInterface.doFavorite(id);
        call.enqueue(new Callback<Favorite>() {
            @Override
            public void onResponse(@NonNull Call<Favorite> call, @NonNull Response<Favorite> response) {
                Favorite favorite = response.body();
                Toast.makeText(context, "" + favorite.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(@NonNull Call<Favorite> call, @NonNull Throwable t) {

            }
        });

    }

    private void setViewcartBottomLayout(AddCart addCart) {
        priceAmount = 0;
        itemQuantity = 0;
        itemCount = 0;
        //get Item Count
        itemCount = addCart.getProductList().size();
        for (int i = 0; i < itemCount; i++) {
            //Get Total item Quantity
            itemQuantity = itemQuantity + addCart.getProductList().get(i).getQuantity();
            //Get product price
            if (addCart.getProductList().get(i).getProduct().getPrices() != null)
                priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity() * addCart.getProductList().get(i).getProduct().getPrices().getOrignalPrice());
            if (addCart.getProductList().get(i).getCartAddons() != null && !addCart.getProductList().get(i).getCartAddons().isEmpty()) {
                for (int j = 0; j < addCart.getProductList().get(i).getCartAddons().size(); j++) {
                    priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity() * (addCart.getProductList().get(i).getCartAddons().get(j).getQuantity() *
                            addCart.getProductList().get(i).getCartAddons().get(j).getAddonProduct().getPrice()));
                }
            }
        }
        GlobalData.notificationCount = itemQuantity;
        if (itemQuantity == 0) {
            viewCartLayout.setVisibility(View.GONE);
            // Start animation
            viewCartLayout.startAnimation(slide_down);
        } else if (itemQuantity == 1) {
            if (shops!=null&&!shops.getId().equals(GlobalData.addCart.getProductList().get(0).getProduct().getShopId())) {
                viewCartShopName.setVisibility(View.VISIBLE);
                viewCartShopName.setText("From : " + GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
            } else
                viewCartShopName.setVisibility(View.GONE);
            String currency = addCart.getProductList().get(0).getProduct().getPrices().getCurrency();
            itemText.setText("" + itemQuantity + " Item | " + currency + "" +String.format("%.2f", priceAmount) );
            if (viewCartLayout.getVisibility() == View.GONE) {
                // Start animation
                viewCartLayout.setVisibility(View.VISIBLE);
                viewCartLayout.startAnimation(slide_up);
            }
        } else {
            if (shops!=null&&!shops.getId().equals( GlobalData.addCart.getProductList().get(0).getProduct().getShopId())) {
                viewCartShopName.setVisibility(View.VISIBLE);
                viewCartShopName.setText("From : " + GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
            } else
                viewCartShopName.setVisibility(View.GONE);

            String currency = addCart.getProductList().get(0).getProduct().getPrices().getCurrency();
            itemText.setText("" + itemQuantity + " Items | " + currency + "" + String.format("%.2f", priceAmount));
            if (viewCartLayout.getVisibility() == View.GONE) {
                // Start animation
                viewCartLayout.setVisibility(View.VISIBLE);
                viewCartLayout.startAnimation(slide_up);
            }

        }
    }

    private void getCategories(HashMap<String, String> map) {
        skeleton.show();
        Call<ShopDetail> call = apiInterface.getCategories(map);
        call.enqueue(new Callback<ShopDetail>() {
            @Override
            public void onResponse(@NonNull Call<ShopDetail> call, @NonNull Response<ShopDetail> response) {
                skeleton.hide();
                if (response.isSuccessful()) {
                    categoryList = new ArrayList<>();
                    categoryList.clear();
                    Category category = new Category();
                    featureProductList = new ArrayList<>();
                    featureProductList = response.body().getFeaturedProducts();
                    category.setName(getResources().getString(R.string.featured_products));
                    category.setProducts(featureProductList);
                    categoryList.add(category);
                    categoryList.addAll(response.body().getCategories());
                    GlobalData.categoryList = categoryList;
                    GlobalData.selectedShop.setCategories(categoryList);
                    catagoeryAdapter = new HotelCatagoeryAdapter(context, activity, categoryList);
                    accompanimentDishesRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
                    accompanimentDishesRv.setItemAnimator(new DefaultItemAnimator());
                    accompanimentDishesRv.setAdapter(catagoeryAdapter);
                    if (GlobalData.addCart != null && GlobalData.addCart.getProductList().size() != 0) {
                        setViewcartBottomLayout(GlobalData.addCart);
                    } else {
                        viewCartLayout.setVisibility(View.GONE);
                    }
                    catagoeryAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ShopDetail> call, @NonNull Throwable t) {

            }
        });


    }

    /*@Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }*/

    @Override
    protected void onResume() {
        super.onResume();
        if (bottomSheetDialogFragment != null)
            bottomSheetDialogFragment.dismiss();

        if (!isCategory) {
            if (connectionHelper.isConnectingToInternet()) {
                //get User Profile Data
                if (GlobalData.profileModel != null) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("shop", String.valueOf(shops.getId()));
                    map.put("user_id", String.valueOf(GlobalData.profileModel.getId()));
                    getCategories(map);
                    getRestaurantByShopId();
                } else {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("shop", String.valueOf(shops.getId()));
                    getCategories(map);
                }
            } else {
                Utils.displayMessage(activity, context, getString(R.string.oops_connect_your_internet));
            }

            if (GlobalData.addCart != null && GlobalData.addCart.getProductList().size() != 0) {
                viewCartLayout.setVisibility(View.VISIBLE);
            } else {
                viewCartLayout.setVisibility(View.GONE);
            }
        } else
        if (GlobalData.profileModel != null)
            getCategoryData();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.anim_nothing, R.anim.slide_out_right);
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(verticalOffset) / (float) maxScroll;

        if (percentage == 1f && isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.VISIBLE);
            isHideToolbarView = !isHideToolbarView;

        } else if (percentage < 1f && !isHideToolbarView) {
            toolbarHeaderView.setVisibility(View.GONE);
            isHideToolbarView = !isHideToolbarView;
        }
    }
}

