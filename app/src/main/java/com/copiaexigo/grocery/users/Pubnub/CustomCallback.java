package com.copiaexigo.grocery.users.Pubnub;

/**
 * Created by Anjith Sasindran
 * on 11-Oct-15.
 */
public interface CustomCallback {

    //0 for LoginFragment & 1 for ChatFragment
    void loginActivity(int LOGIN_STATE);
}
