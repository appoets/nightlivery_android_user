package com.copiaexigo.grocery.users.models.specifiedshop;

import java.io.Serializable;
import java.util.List;

import com.copiaexigo.grocery.users.models.Cart;
import com.copiaexigo.grocery.users.models.Favorite;
import com.google.gson.annotations.SerializedName;

public class ProductItem implements Serializable {

	@SerializedName("out_of_stock")
	private String outOfStock;

	@SerializedName("featured")
	private Integer featured;

	@SerializedName("featured_images")
	private List<FeaturedImagesItem> featuredImages;

	@SerializedName("images")
	private List<ImagesItem> images;

	@SerializedName("max_quantity")
	private Integer maxQuantity;

	@SerializedName("addons")
	private List<Object> addons;

	@SerializedName("featured_position")
	private Integer featuredPosition;

	@SerializedName("description")
	private String description;

	@SerializedName("cuisine_id")
	private Object cuisineId;

	@SerializedName("avalability")
	private Integer avalability;

	@SerializedName("food_type")
	private String foodType;

	@SerializedName("cart")
	private List<Cart> cart;

	@SerializedName("shop_id")
	private Integer shopId;

	@SerializedName("addon_status")
	private Integer addonStatus;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private Integer id;

	@SerializedName("position")
	private Integer position;

	@SerializedName("prices")
	private Prices prices;

	@SerializedName("status")
	private String status;
	@SerializedName("favorite")
	private Favorite favorite;

	public Favorite getFavorite() {
		return favorite;
	}

	public void setFavorite(Favorite favorite) {
		this.favorite = favorite;
	}

	public void setOutOfStock(String outOfStock){
		this.outOfStock = outOfStock;
	}

	public String getOutOfStock(){
		return outOfStock;
	}

	public void setFeatured(Integer featured){
		this.featured = featured;
	}

	public Integer getFeatured(){
		return featured;
	}

	public void setFeaturedImages(List<FeaturedImagesItem> featuredImages){
		this.featuredImages = featuredImages;
	}

	public List<FeaturedImagesItem> getFeaturedImages(){
		return featuredImages;
	}

	public void setImages(List<ImagesItem> images){
		this.images = images;
	}

	public List<ImagesItem> getImages(){
		return images;
	}

	public void setMaxQuantity(Integer maxQuantity){
		this.maxQuantity = maxQuantity;
	}

	public Integer getMaxQuantity(){
		return maxQuantity;
	}

	public void setAddons(List<Object> addons){
		this.addons = addons;
	}

	public List<Object> getAddons(){
		return addons;
	}

	public void setFeaturedPosition(Integer featuredPosition){
		this.featuredPosition = featuredPosition;
	}

	public Integer getFeaturedPosition(){
		return featuredPosition;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setCuisineId(Object cuisineId){
		this.cuisineId = cuisineId;
	}

	public Object getCuisineId(){
		return cuisineId;
	}

	public void setAvalability(Integer avalability){
		this.avalability = avalability;
	}

	public Integer getAvalability(){
		return avalability;
	}

	public void setFoodType(String foodType){
		this.foodType = foodType;
	}

	public String getFoodType(){
		return foodType;
	}

	public void setCart(List<Cart> cart){
		this.cart = cart;
	}

	public List<Cart> getCart(){
		return cart;
	}

	public void setShopId(Integer shopId){
		this.shopId = shopId;
	}

	public Integer getShopId(){
		return shopId;
	}

	public void setAddonStatus(Integer addonStatus){
		this.addonStatus = addonStatus;
	}

	public Integer getAddonStatus(){
		return addonStatus;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(Integer id){
		this.id = id;
	}

	public Integer getId(){
		return id;
	}

	public void setPosition(Integer position){
		this.position = position;
	}

	public Integer getPosition(){
		return position;
	}

	public void setPrices(Prices prices){
		this.prices = prices;
	}

	public Prices getPrices(){
		return prices;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}