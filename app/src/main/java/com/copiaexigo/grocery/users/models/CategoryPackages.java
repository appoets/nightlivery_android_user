
package com.copiaexigo.grocery.users.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

import com.copiaexigo.grocery.users.models.specifiedshop.ProductItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryPackages {

    @SerializedName("products")
    @Expose
    private List<ProductItem> products = null;

    public List<ProductItem> getProducts() {
        return products;
    }

    public void setProducts(List<ProductItem> products) {
        this.products = products;
    }
}


/*
class CategoryProduct implements Parcelable{

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("shop_id")
    @Expose
    private Integer shopId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("brand")
    @Expose
    private Object brand;
    @SerializedName("nutrition")
    @Expose
    private Object nutrition;
    @SerializedName("ingredients")
    @Expose
    private Object ingredients;
    @SerializedName("directions")
    @Expose
    private Object directions;
    @SerializedName("warnings")
    @Expose
    private Object warnings;
    @SerializedName("product_info_img")
    @Expose
    private Object productInfoImg;
    @SerializedName("position")
    @Expose
    private Object position;
    @SerializedName("food_type")
    @Expose
    private String foodType;
    @SerializedName("avalability")
    @Expose
    private Integer avalability;
    @SerializedName("max_quantity")
    @Expose
    private Integer maxQuantity;
    @SerializedName("featured")
    @Expose
    private Integer featured;
    @SerializedName("featured_position")
    @Expose
    private Integer featuredPosition;
    @SerializedName("addon_status")
    @Expose
    private Integer addonStatus;
    @SerializedName("cuisine_id")
    @Expose
    private Object cuisineId;
    @SerializedName("out_of_stock")
    @Expose
    private String outOfStock;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("prices")
    @Expose
    private Prices prices;
    @SerializedName("images")
    @Expose
    private List<Image> images = null;

    protected CategoryProduct(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        if (in.readByte() == 0) {
            shopId = null;
        } else {
            shopId = in.readInt();
        }
        name = in.readString();
        description = in.readString();
        foodType = in.readString();
        if (in.readByte() == 0) {
            avalability = null;
        } else {
            avalability = in.readInt();
        }
        if (in.readByte() == 0) {
            maxQuantity = null;
        } else {
            maxQuantity = in.readInt();
        }
        if (in.readByte() == 0) {
            featured = null;
        } else {
            featured = in.readInt();
        }
        if (in.readByte() == 0) {
            featuredPosition = null;
        } else {
            featuredPosition = in.readInt();
        }
        if (in.readByte() == 0) {
            addonStatus = null;
        } else {
            addonStatus = in.readInt();
        }
        outOfStock = in.readString();
        status = in.readString();
    }

    public static final Creator<CategoryProduct> CREATOR = new Creator<CategoryProduct>() {
        @Override
        public CategoryProduct createFromParcel(Parcel in) {
            return new CategoryProduct(in);
        }

        @Override
        public CategoryProduct[] newArray(int size) {
            return new CategoryProduct[size];
        }
    };

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getBrand() {
        return brand;
    }

    public void setBrand(Object brand) {
        this.brand = brand;
    }

    public Object getNutrition() {
        return nutrition;
    }

    public void setNutrition(Object nutrition) {
        this.nutrition = nutrition;
    }

    public Object getIngredients() {
        return ingredients;
    }

    public void setIngredients(Object ingredients) {
        this.ingredients = ingredients;
    }

    public Object getDirections() {
        return directions;
    }

    public void setDirections(Object directions) {
        this.directions = directions;
    }

    public Object getWarnings() {
        return warnings;
    }

    public void setWarnings(Object warnings) {
        this.warnings = warnings;
    }

    public Object getProductInfoImg() {
        return productInfoImg;
    }

    public void setProductInfoImg(Object productInfoImg) {
        this.productInfoImg = productInfoImg;
    }

    public Object getPosition() {
        return position;
    }

    public void setPosition(Object position) {
        this.position = position;
    }

    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public Integer getAvalability() {
        return avalability;
    }

    public void setAvalability(Integer avalability) {
        this.avalability = avalability;
    }

    public Integer getMaxQuantity() {
        return maxQuantity;
    }

    public void setMaxQuantity(Integer maxQuantity) {
        this.maxQuantity = maxQuantity;
    }

    public Integer getFeatured() {
        return featured;
    }

    public void setFeatured(Integer featured) {
        this.featured = featured;
    }

    public Integer getFeaturedPosition() {
        return featuredPosition;
    }

    public void setFeaturedPosition(Integer featuredPosition) {
        this.featuredPosition = featuredPosition;
    }

    public Integer getAddonStatus() {
        return addonStatus;
    }

    public void setAddonStatus(Integer addonStatus) {
        this.addonStatus = addonStatus;
    }

    public Object getCuisineId() {
        return cuisineId;
    }

    public void setCuisineId(Object cuisineId) {
        this.cuisineId = cuisineId;
    }

    public String getOutOfStock() {
        return outOfStock;
    }

    public void setOutOfStock(String outOfStock) {
        this.outOfStock = outOfStock;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Prices getPrices() {
        return prices;
    }

    public void setPrices(Prices prices) {
        this.prices = prices;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        if (id == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(id);
        }
        if (shopId == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(shopId);
        }
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(foodType);
        if (avalability == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(avalability);
        }
        if (maxQuantity == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(maxQuantity);
        }
        if (featured == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(featured);
        }
        if (featuredPosition == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(featuredPosition);
        }
        if (addonStatus == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(addonStatus);
        }
        parcel.writeString(outOfStock);
        parcel.writeString(status);
    }
}*/
