package com.copiaexigo.grocery.users.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.copiaexigo.grocery.users.HomeActivity;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.adapter.ProductImageAdapter;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.ConnectionHelper;
import com.copiaexigo.grocery.users.helper.CustomDialog;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.AddCart;
import com.copiaexigo.grocery.users.models.ClearCart;
import com.copiaexigo.grocery.users.models.Favorite;
import com.copiaexigo.grocery.users.models.Shop;
import com.copiaexigo.grocery.users.models.specifiedshop.Prices;
import com.copiaexigo.grocery.users.models.specifiedshop.ProductItem;
import com.copiaexigo.grocery.users.utils.CircleIndicator;
import com.copiaexigo.grocery.users.utils.Utils;
import com.google.android.gms.common.util.CollectionUtils;
import com.google.gson.Gson;
import com.sackcentury.shinebuttonlib.ShineButton;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copiaexigo.grocery.users.helper.GlobalData.notificationCount;
import static com.copiaexigo.grocery.users.helper.GlobalData.profileModel;
import static com.copiaexigo.grocery.users.helper.GlobalData.selectedShop;
import static com.copiaexigo.grocery.users.helper.GlobalData.selectedShopId;

public class ProductFullDetailActivity extends AppCompatActivity implements
        ViewPager.OnPageChangeListener {

    private ProductItem productItem;
    @BindView(R.id.view_pager)
    public ViewPager viewPager;
    @BindView(R.id.indicator)
    public CircleIndicator mIndicator;
    @BindView(R.id.tvTitle)
    public TextView tvTitle;
    @BindView(R.id.tvDescription)
    public TextView tvDescription;
    @BindView(R.id.tvOfferPrice)
    public TextView tvOfferPrice;
    @BindView(R.id.tvOriginalPrice)
    public TextView tvOriginalPrice;
    @BindView(R.id.tvRegularPrice)
    public TextView tvRegularPrice;

    @BindView(R.id.add_card_text_layout)
    public TextView cardAddDetailLayout;
    @BindView(R.id.card_value)
    public TextView cardTextValue;

    public int itemCount = 1;
    public static Shop shops;
    boolean isFavourite = false;
    @BindView(R.id.heart_btn)
    ShineButton heartBtn;
    ConnectionHelper connectionHelper;
    Context context;
    Activity activity;
    CustomDialog customDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_details_activity);
        ButterKnife.bind(this);

        context = ProductFullDetailActivity.this;
        activity = ProductFullDetailActivity.this;
        connectionHelper = new ConnectionHelper(context);
        customDialog = new CustomDialog(context);
        initIntentData();
    }

    private void initIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            productItem = new Gson().fromJson(bundle.getString("productItem"), ProductItem.class);

        }

        viewPager.setAdapter(new ProductImageAdapter(productItem.getImages(), ProductFullDetailActivity.this));
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(ProductFullDetailActivity.this);
        mIndicator.setViewPager(viewPager);
        Prices price = productItem.getPrices();
        tvTitle.setText(productItem.getName());
        tvDescription.setText(productItem.getDescription());
        tvOriginalPrice.setText(String.format(Locale.getDefault(), "%s%.2f", price.getCurrency(), price.getOrignalPrice()));
        tvOfferPrice.setText(String.format(Locale.getDefault(), "%s %s%.2f", getString(R.string.save), price.getCurrency(), (price.getOrignalPrice() - price.getPrice())));
        tvRegularPrice.setText(String.format(Locale.getDefault(), "%s%.2f", price.getCurrency(), price.getPrice()));
        shops = GlobalData.selectedShop;
        if (productItem.getCart().size()>0){
            cardTextValue.setText(String.valueOf(productItem.getCart().get(0).getQuantity()));
        }
        if (shops != null) {
            if (heartBtn != null)
                heartBtn.init(this);
            if (productItem.getFavorite() != null || isFavourite) {
                heartBtn.setChecked(true);
                heartBtn.setTag(1);
            } else {
                heartBtn.setTag(0);
                heartBtn.setChecked(false);
            }
            heartBtn.setShineDistanceMultiple(1.8f);
            heartBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (heartBtn.getTag().equals(0)) {
                        heartBtn.setTag(1);
                        //heartBtn.setShapeResource(R.raw.heart);
                    } else {
                        heartBtn.setTag(0);
                       // heartBtn.setShapeResource(R.raw.icc_heart);

                    }

                }
            });
            heartBtn.setOnCheckStateChangeListener(new ShineButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(View view, boolean checked) {
                    Log.e("HeartButton", "click " + checked);
                    if (connectionHelper.isConnectingToInternet()) {
                        //if (checked) {
                            if (GlobalData.profileModel != null)
                                doFavorite(shops.getId(),productItem.getId());
                            else {
                                startActivity(new Intent(activity, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                                overridePendingTransition(R.anim.slide_in_left, R.anim.anim_nothing);
                                finish();
                            }
                       /* } else {
                            deleteFavorite(shops.getId());
                        }*/
                    } else {
                        Utils.displayMessage(activity,context, getString(R.string.oops_connect_your_internet));
                    }
                }
            });
        }

    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }
    private void doFavorite(Integer id, int product_id) {
        Call<Favorite> call = apiInterface.doFavorite(id,product_id);
        call.enqueue(new Callback<Favorite>() {
            @Override
            public void onResponse(@NonNull Call<Favorite> call, @NonNull Response<Favorite> response) {
                Favorite favorite = response.body();
                Toast.makeText(context, "" + favorite.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(@NonNull Call<Favorite> call, @NonNull Throwable t) {

            }
        });

    }


    @OnClick({R.id.card_add_btn, R.id.card_minus_btn,R.id.add_card_text_layout,R.id.close_img})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_card_text_layout:
                addCartItem();
                break;
            case R.id.close_img:
                finish();
                break;
            case R.id.card_add_btn:
                int countValue = Integer.parseInt(cardTextValue.getText().toString()) + 1;
                itemCount = itemCount + 1;
                cardTextValue.setText(String.valueOf(countValue));
                break;
            case R.id.card_minus_btn:
                if (cardTextValue.getText().toString().equalsIgnoreCase("1")) {
                } else {
                    int countMinusValue = Integer.parseInt(cardTextValue.getText().toString()) - 1;
                    itemCount = itemCount - 1;
                    cardTextValue.setText(String.valueOf(countMinusValue));
                }
                break;
        }
    }


    public ProductItem product;
    public ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);

    public double priceAmount = 0;
    public int itemQuantity = 0;
    public AddCart addCart;
    public Shop currentShop = new Shop();

    public void addCart(HashMap<String, String> map) {
        if (!customDialog.isShowing()) {
            customDialog = new CustomDialog(context);
            customDialog.setCancelable(false);
            customDialog.show();
        }
        Call<AddCart> call = apiInterface.postAddCart(map);
        call.enqueue(new Callback<AddCart>() {
            @Override
            public void onResponse(Call<AddCart> call, Response<AddCart> response) {
                customDialog.dismiss();
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(ProductFullDetailActivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ProductFullDetailActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    GlobalData.addCartShopId = selectedShopId;
                    addCart = response.body();
                    GlobalData.addCart = response.body();

                    priceAmount = 0;
                    itemQuantity = 0;
                    itemCount = 0;
                    //get Item Count
                    itemCount = addCart.getProductList().size();
                    for (int i = 0; i < itemCount; i++) {
                        //Get Total item Quantity
                        itemQuantity = itemQuantity + addCart.getProductList().get(i).getQuantity();
                        //Get addon price
                        if (addCart.getProductList().get(i).getProduct().getPrices().getPrice() != null)
                            priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity() * addCart.getProductList().get(i).getProduct().getPrices().getPrice());
                    }

                    notificationCount = itemQuantity;
                    HomeActivity.updateNotificationCount(ProductFullDetailActivity.this, notificationCount);

                    startActivity(new Intent(ProductFullDetailActivity.this, ViewCartActivity.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                }
            }

            @Override
            public void onFailure(Call<AddCart> call, Throwable t) {
                customDialog.dismiss();
            }
        });

    }

    private void clearCart() {
        Call<ClearCart> call = apiInterface.clearCart();
        call.enqueue(new Callback<ClearCart>() {
            @Override
            public void onResponse(Call<ClearCart> call, Response<ClearCart> response) {

                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(ProductFullDetailActivity.this, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        Toast.makeText(ProductFullDetailActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    selectedShop = HotelViewActivity.shops;
                    GlobalData.addCart.getProductList().clear();
                    GlobalData.notificationCount = 0;
                    if (product.getAddons() != null && product.getAddons().size() != 0) {
                        startActivity(new Intent(ProductFullDetailActivity.this, ProductDetailActivity.class));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                    } else {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("product_id", product.getId().toString());
                        map.put("quantity", "1");
                        Log.e("AddCart_Text", map.toString());
                        addCart(map);
                    }
                }

            }

            @Override
            public void onFailure(Call<ClearCart> call, Throwable t) {
                Toast.makeText(ProductFullDetailActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                GlobalData.addCartShopId = selectedShop.getId();
            }
        });

    }

    private void addCartItem() {
        product = productItem;
        selectedShopId = productItem.getShopId();
        if (profileModel != null) {
            if (Utils.isShopChanged(product.getShopId())) {
                String message = String.format(getResources().getString(R.string.reorder_confirm_message), product.getName(), GlobalData.addCart.getProductList().get(0).getProduct().getShop().getName());
                AlertDialog.Builder builder = new AlertDialog.Builder(ProductFullDetailActivity.this);
                builder.setTitle(getResources().getString(R.string.replace_cart_item))
                        .setMessage(message)
                        .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // continue with delete
                                clearCart();
                            }
                        })
                        .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                dialog.dismiss();

                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
                Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
                nbutton.setTextColor(ContextCompat.getColor(this, R.color.theme));
                nbutton.setTypeface(nbutton.getTypeface(), Typeface.BOLD);
                Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
                pbutton.setTextColor(ContextCompat.getColor(this, R.color.theme));
                pbutton.setTypeface(pbutton.getTypeface(), Typeface.BOLD);
            } else {
                if (product.getAddons() != null && product.getAddons().size() != 0) {
                    startActivity(new Intent(this, ProductDetailActivity.class));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                } else {
                    int cartId = 0;
                    if (!CollectionUtils.isEmpty(GlobalData.addCart.getProductList())) {
                        for (int i = 0; i < GlobalData.addCart.getProductList().size(); i++) {
                            if (GlobalData.addCart.getProductList().get(i).getProductId().equals(product.getId())) {
                                cartId = GlobalData.addCart.getProductList().get(i).getId();
                            }
                        }
                    }
                    HashMap<String, String> map = new HashMap<>();
                    map.put("product_id", product.getId().toString());
                    map.put("quantity", cardTextValue.getText().toString());
                    if (cartId>0)
                    map.put("cart_id", String.valueOf(cartId));
                    addCart(map);
                }
            }
        } else {
            startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            overridePendingTransition(R.anim.slide_in_left, R.anim.anim_nothing);
            finish();
            Toast.makeText(this, getResources().getString(R.string.please_login_and_order_dishes), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPageSelected(int i) {

    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }
}
