package com.copiaexigo.grocery.users.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.copiaexigo.grocery.users.HomeActivity;
import com.copiaexigo.grocery.users.R;
import com.copiaexigo.grocery.users.activities.AccountPaymentActivity;
import com.copiaexigo.grocery.users.activities.PromotionActivity;
import com.copiaexigo.grocery.users.activities.SetDeliveryLocationActivity;
import com.copiaexigo.grocery.users.adapter.ViewCartAdapter;
import com.copiaexigo.grocery.users.build.api.ApiClient;
import com.copiaexigo.grocery.users.build.api.ApiInterface;
import com.copiaexigo.grocery.users.helper.ConnectionHelper;
import com.copiaexigo.grocery.users.helper.CustomDialog;
import com.copiaexigo.grocery.users.helper.GlobalData;
import com.copiaexigo.grocery.users.models.AddCart;
import com.copiaexigo.grocery.users.models.Cart;
import com.copiaexigo.grocery.users.utils.Utils;
import com.ethanhua.skeleton.Skeleton;
import com.ethanhua.skeleton.ViewSkeletonScreen;
import com.robinhood.ticker.TickerUtils;

import org.json.JSONObject;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.copiaexigo.grocery.users.adapter.ViewCartAdapter.bottomSheetDialogFragment;

@SuppressLint({"SetTextI18n", "NonConstantResourceId"})
public class CartFragment extends Fragment {

    //Animation number
    private static final char[] NUMBER_LIST = TickerUtils.getDefaultNumberList();

    private static final int PROMOCODE_APPLY = 201;

    public static RelativeLayout dataLayout;
    public static RelativeLayout errorLayout;
    public static TextView itemTotalAmount;
    public static TextView deliveryCharges;
    public static TextView promoCodeApply;
    public static TextView discountAmount;
    public static TextView serviceTax;
    public static TextView payAmount;
    //Orderitem List
    public static List<Cart> viewCartItemList;
    public static double deliveryChargeValue = 0;
    public static double tax = 0;
    public static ViewCartAdapter viewCartAdapter;
    public static boolean isAddressSelected = false;
    public static boolean isTimeSelected = false;
    public static HashMap<String, String> checkoutMap;
    public String schedule_time = "";
    public String schedule_date = "";
    @BindView(R.id.re)
    RelativeLayout re;
    @BindView(R.id.order_item_rv)
    RecyclerView orderItemRv;
    @BindView(R.id.total_amount)
    TextView totalAmount;
    @BindView(R.id.buttonLayout)
    LinearLayout buttonLayout;
    @BindView(R.id.address_detail)
    TextView addressDetail;
    @BindView(R.id.address_delivery_time)
    TextView addressDeliveryTime;
    @BindView(R.id.add_address_txt)
    TextView addAddressTxt;
    @BindView(R.id.bottom_layout)
    LinearLayout bottomLayout;
    @BindView(R.id.rlTimeLayout)
    RelativeLayout rlTimeLayout;
    @BindView(R.id.location_info_layout)
    RelativeLayout locationInfoLayout;
    @BindView(R.id.location_error_layout)
    RelativeLayout locationErrorLayout;
    @BindView(R.id.restaurant_image)
    CircleImageView restaurantImage;
    @BindView(R.id.restaurant_name)
    TextView restaurantName;
    @BindView(R.id.restaurant_description)
    TextView restaurantDescription;
    @BindView(R.id.btnProceedToPayment)
    Button proceedToPayBtn;
    @BindView(R.id.error_layout_description)
    TextView errorLayoutDescription;
    @BindView(R.id.use_wallet_chk_box)
    CheckBox useWalletChkBox;
    @BindView(R.id.amount_txt)
    TextView amountTxt;
    @BindView(R.id.custom_notes)
    TextView customNotes;
    @BindView(R.id.pickupDelivery)
    LinearLayout pickupDelivery;
    @BindView(R.id.wallet_layout)
    LinearLayout walletLayout;
    @BindView(R.id.scheduledContinue)
    LinearLayout scheduledContinue;
    @BindView(R.id.llProceedToPayment)
    LinearLayout llProceedToPayment;
    @BindView(R.id.lnrPromocodeAmount)
    LinearLayout lnrPromocodeAmount;
    @BindView(R.id.change_promo_code)
    TextView removePromoCode;
    AnimatedVectorDrawableCompat avdProgress;
    Fragment orderFullViewFragment;
    FragmentManager fragmentManager;
    double priceAmount = 0;
    double discount = 0;
    int itemCount = 0;
    int itemQuantity = 0;
    int ADDRESS_SELECTION = 1;
    ApiInterface apiInterface = ApiClient.getRetrofit().create(ApiInterface.class);
    CustomDialog customDialog;
    ViewSkeletonScreen skeleton;
    ConnectionHelper connectionHelper;
    Activity activity;
    Calendar myCalendar = Calendar.getInstance();
    String promo_code = "", from_ = "";
    HashMap<String, String> params = new HashMap<>();
    private Context context;
    private ViewGroup toolbar;
    private View toolbarLayout;

    @BindView(R.id.tip_radio_group)
    RadioGroup tipRadioGroup;

    @BindView(R.id.rb_no_tip)
    RadioButton rbNoTip;
    @BindView(R.id.rb_five)
    RadioButton rbFive;
    @BindView(R.id.rb_ten)
    RadioButton rbTen;

    @BindView(R.id.et_tip)
    EditText customTip;

    @BindView(R.id.tv_tip_amount)
    TextView tipAmount;

    private RadioButton selectedTip;
    private Double tipForProvider = 0.00;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = getContext();
        this.activity = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        ButterKnife.bind(this, view);
        connectionHelper = new ConnectionHelper(context);
        //GlobalData.addCart = null;

        /*  Intialize Global Values*/
        itemTotalAmount = view.findViewById(R.id.item_total_amount);
        deliveryCharges = view.findViewById(R.id.delivery_charges);
        promoCodeApply = view.findViewById(R.id.promo_code_apply);
        discountAmount = view.findViewById(R.id.discount_amount);
        serviceTax = view.findViewById(R.id.service_tax);
        payAmount = view.findViewById(R.id.total_amount);
        dataLayout = view.findViewById(R.id.data_layout);
        errorLayout = view.findViewById(R.id.error_layout);
        //GlobalData.addCart = null;


        HomeActivity.updateNotificationCount(context, 0);
        customDialog = new CustomDialog(context);

        skeleton = Skeleton.bind(dataLayout)
                .load(R.layout.skeleton_fragment_cart)
                .show();
        viewCartItemList = new ArrayList<>();
        //Offer Restaurant Adapter
        orderItemRv.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        orderItemRv.setItemAnimator(new DefaultItemAnimator());
        orderItemRv.setHasFixedSize(false);
        orderItemRv.setNestedScrollingEnabled(false);

        //Intialize address Value
        if (GlobalData.getInstance().selectedAddress != null && GlobalData.getInstance().selectedAddress.getLandmark() != null) {
            if (GlobalData.getInstance().addressList.getAddresses().size() == 1)
                addAddressTxt.setText(getString(R.string.add_address));
            else
                addAddressTxt.setText(getString(R.string.change_address));
            addressDetail.setText(GlobalData.getInstance().selectedAddress.getMapAddress());
            //if (viewCartItemList != null && viewCartItemList.size() != 0)
            //addressDeliveryTime.setText(String.valueOf(viewCartItemList.get(0).getProduct().getShop().getEstimatedDeliveryTime()) + " Mins");
        } else if (GlobalData.getInstance().addressList != null) {
            locationInfoLayout.setVisibility(View.VISIBLE);
            locationErrorLayout.setVisibility(View.GONE);
        } else {
            locationErrorLayout.setVisibility(View.VISIBLE);
            locationInfoLayout.setVisibility(View.GONE);
        }


        promoCodeApply.setOnClickListener(view1 -> {
            startActivityForResult(new Intent(activity, PromotionActivity.class)
                    .putExtra("tag", "CartFragment"), PROMOCODE_APPLY);
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
        });

        removePromoCode.setOnClickListener(v -> {
            startActivityForResult(new Intent(activity, PromotionActivity.class)
                    .putExtra("tag", "CartFragment"), PROMOCODE_APPLY);
            activity.overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
        });

        customTip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.toString().isEmpty()){
                    tipAmount.setText("$0");
                    tipRadioGroup.check(R.id.rb_no_tip);
                }
                else if (s.toString().length() > 0 && Double.parseDouble(s.toString()) > 0.00) {
                    rbNoTip.setChecked(false);
                    rbFive.setChecked(false);
                    rbTen.setChecked(false);
                    tipAmount.setText(GlobalData.currencySymbol + Double.parseDouble(s.toString()));
                    payAmount.setText((GlobalData.currencySymbol + String.format(Locale.getDefault(), "%.2f", GlobalData.cartItemTotal + Double.parseDouble(s.toString()))));
                    tipForProvider = Double.parseDouble(s.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        tipRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            selectedTip = group.findViewById(checkedId);
            switch (selectedTip.getId()) {
                case R.id.rb_no_tip:
                    tipForProvider = 0.00;
                    tipAmount.setText(GlobalData.currencySymbol + tipForProvider);
                    payAmount.setText((GlobalData.currencySymbol + String.format(Locale.getDefault(), "%.2f", GlobalData.cartItemTotal + tipForProvider)));
                    customTip.getText().clear();
                    customTip.setEnabled(false);
                    break;
                case R.id.rb_five:
                    tipForProvider = GlobalData.cartItemTotal * .05;
                    tipAmount.setText(GlobalData.currencySymbol + String.format(Locale.getDefault(), "%.2f", tipForProvider));
                    payAmount.setText((GlobalData.currencySymbol + String.format(Locale.getDefault(), "%.2f", GlobalData.cartItemTotal + tipForProvider)));
                    customTip.getText().clear();
                    customTip.setEnabled(false);
                    break;
                case R.id.rb_ten:
                    tipForProvider = GlobalData.cartItemTotal * .10;
                    tipAmount.setText(GlobalData.currencySymbol + String.format(Locale.getDefault(), "%.2f", tipForProvider));
                    payAmount.setText((GlobalData.currencySymbol + String.format(Locale.getDefault(), "%.2f", GlobalData.cartItemTotal + tipForProvider)));
                    customTip.getText().clear();
                    customTip.setEnabled(false);
                    break;
                case R.id.rb_custom:
                    customTip.setEnabled(true);
                    customTip.requestFocus();
                    break;
            }
        });

        return view;
    }

    private void getViewCart() {
        Call<AddCart> call = apiInterface.getViewCart(params);
        call.enqueue(new Callback<AddCart>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<AddCart> call, Response<AddCart> response) {
                skeleton.hide();
                if (response != null && !response.isSuccessful() && response.errorBody() != null) {
                    errorLayout.setVisibility(View.VISIBLE);
                    dataLayout.setVisibility(View.GONE);
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
//                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    customDialog.dismiss();
                    //get Item Count
                    itemCount = response.body().getProductList().size();
                    GlobalData.getInstance().notificationCount = response.body().getProductList().size();
                    if (itemCount == 0) {
                        errorLayout.setVisibility(View.VISIBLE);
                        dataLayout.setVisibility(View.GONE);
                        GlobalData.addCart = response.body();
                        GlobalData.addCart = null;
                        isAddressSelected = false;
                        isTimeSelected = false;
                    } else {
                        AddCart addCart = response.body();
                        GlobalData.addCart = response.body();
                        errorLayout.setVisibility(View.GONE);
                        dataLayout.setVisibility(View.VISIBLE);
                        for (int i = 0; i < itemCount; i++) {
                            //Get Total item Quantity
                            itemQuantity = itemQuantity + response.body().getProductList().get(i).getQuantity();
                            //Get product price
                            if (response.body().getProductList().get(i).getProduct().getPrices().getOrignalPrice() != null)
                                priceAmount = priceAmount + (response.body().getProductList().get(i).getQuantity() *
                                        response.body().getProductList().get(i).getProduct().getPrices().getOrignalPrice());

                            if (addCart.getProductList().get(i).getCartAddons() != null && !addCart.getProductList().get(i).getCartAddons().isEmpty()) {
                                for (int j = 0; j < addCart.getProductList().get(i).getCartAddons().size(); j++) {
                                    priceAmount = priceAmount + (addCart.getProductList().get(i).getQuantity() * (addCart.getProductList()
                                            .get(i).getCartAddons().get(j).getQuantity() *
                                            addCart.getProductList().get(i).getCartAddons().get(j).getAddonProduct().getPrice()));
                                }
                            }
                        }
                        GlobalData.notificationCount = itemQuantity;
                        GlobalData.getInstance().addCartShopId = response.body().getProductList().get(0).getProduct().getShopId();
                        //Set Payment details
                        String currency = response.body().getProductList().get(0).getProduct().getPrices().getCurrency();
//                        itemTotalAmount.setText(currency + "" + String.format("%.2f", priceAmount));
                        itemTotalAmount.setText(currency + "" + String.format("%.2f", response.body().getTotalPrice()));
                        GlobalData.cartItemTotal = response.body().getPayable();
                        if (response.body().getProductList().get(0).getProduct().getShop().getOfferMinAmount() != 0) {
                            if (response.body().getProductList().get(0).getProduct().getShop().getOfferMinAmount() < priceAmount) {
                                int offerPercentage = 0;
                                if (response.body().getProductList().get(0).getProduct().getShop().getOfferPercent() != null)
                                    offerPercentage = response.body().getProductList().get(0).getProduct().getShop().getOfferPercent();
                                discount = (int) (priceAmount * (offerPercentage * 0.01));
                            }
                        }
//                        discountAmount.setText("- " + currency + "" + String.format("%.2f", discount));
                        discountAmount.setText("- " + currency + "" + String.format("%.2f", response.body().getShopDiscount()));
                        double topPayAmount = priceAmount - discount;
                        double tax = 0.0;
                        tax = response.body().getTax();
                        /*double tax = (int) Math.round(topPayAmount * (response.body().getTaxPercentage() * 0.01));*/
//                        serviceTax.setText(currency + String.format("%.2f", tax));
                        serviceTax.setText(currency + String.format("%.2f", response.body().getTax()));
                        topPayAmount = topPayAmount + response.body().getDeliveryCharges() + tax;
                        payAmount.setText(currency + "" + String.format("%.2f", response.body().getPayable()));
                        //Set Restaurant Details
                        restaurantName.setText(response.body().getProductList().get(0).getProduct().getShop().getName());
                        restaurantDescription.setText(response.body().getProductList().get(0).getProduct().getShop().getDescription());
                        String image_url = response.body().getProductList().get(0).getProduct().getShop().getAvatar();
                        Glide.with(context)
                                .load(image_url)
                                .apply(new RequestOptions()
                                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                                        .placeholder(R.drawable.ic_restaurant_place_holder)
                                        .error(R.drawable.ic_restaurant_place_holder))
                                .into(restaurantImage);
                        deliveryChargeValue = response.body().getDeliveryCharges();
                        deliveryCharges.setText(response.body().getProductList().get(0).getProduct().getPrices().getCurrency()
                                + "" + String.format("%.2f", deliveryChargeValue));
                        viewCartItemList.clear();
                        viewCartItemList = response.body().getProductList();
                        viewCartAdapter = new ViewCartAdapter(viewCartItemList, context);
                        orderItemRv.setAdapter(viewCartAdapter);
                    }

                }
            }

            @Override
            public void onFailure(Call<AddCart> call, Throwable t) {
                errorLayout.setVisibility(View.VISIBLE);
                dataLayout.setVisibility(View.GONE);
            }
        });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onResume() {
        super.onResume();
        priceAmount = 0;
        discount = 0;
        itemCount = 0;
        itemQuantity = 0;
        if (GlobalData.profileModel != null) {
            int money = GlobalData.profileModel.getWalletBalance();
            dataLayout.setVisibility(View.VISIBLE);
            errorLayout.setVisibility(View.GONE);
            skeleton.show();
            errorLayoutDescription.setText(getResources().getString(R.string.cart_error_description));
            if (connectionHelper.isConnectingToInternet()) {
                getViewCart();
            }
            if (connectionHelper.isConnectingToInternet() && GlobalData.addCart == null) {
                getViewCart();
            } else if (GlobalData.addCart != null && GlobalData.addCart.getProductList().size() > 0) {
                String currency = GlobalData.addCart.getProductList().get(0).getProduct().getPrices().getCurrency();

                if (GlobalData.addCart.getTotalPrice() != null) {
                    itemTotalAmount.setText(currency + " " + String.format("%.2f", GlobalData.addCart.getProductList().get(0).getProduct().getPrices().getOrignalPrice()));
                    deliveryCharges.setText(currency + " " + GlobalData.addCart.getDeliveryCharges());
                    discountAmount.setText("- " + currency + "" + GlobalData.addCart.getShopDiscount());
                    serviceTax.setText(currency + " " + GlobalData.addCart.getTax() + "");
                    payAmount.setText(currency + " " + GlobalData.addCart.getPayable().toString());
                }
                dataLayout.setVisibility(View.VISIBLE);
                errorLayout.setVisibility(View.GONE);
                skeleton.hide();
//                promoCodeApply.setText("Applied");
            } else if (!connectionHelper.isConnectingToInternet())
                Utils.displayMessage(activity, context, getString(R.string.oops_connect_your_internet));

            if (money > 0) {
//                amountTxt.setText(numberFormat.format(money));
                amountTxt.setText(GlobalData.currencySymbol + " " + money);
                walletLayout.setVisibility(View.VISIBLE);
            } else {
                walletLayout.setVisibility(View.INVISIBLE);
            }
        } else {
            dataLayout.setVisibility(View.GONE);
            errorLayout.setVisibility(View.VISIBLE);
            errorLayoutDescription.setText(getResources().getString(R.string.please_login_and_order_dishes));
        }
        if (bottomSheetDialogFragment != null)
            bottomSheetDialogFragment.dismiss();

        setFLowVisibility();
    }

    private void setFLowVisibility() {
        if (isAddressSelected) {
            locationInfoLayout.setVisibility(View.VISIBLE);
        } else {
            locationInfoLayout.setVisibility(View.GONE);
        }

        if (isTimeSelected) {
            rlTimeLayout.setVisibility(View.VISIBLE);
        } else {
            rlTimeLayout.setVisibility(View.GONE);
        }

        if (!isTimeSelected && isAddressSelected) {
            pickupDelivery.setVisibility(View.GONE);
            llProceedToPayment.setVisibility(View.GONE);
            scheduledContinue.setVisibility(View.VISIBLE);
        } else if (isTimeSelected && !isAddressSelected) {
            pickupDelivery.setVisibility(View.VISIBLE);
            llProceedToPayment.setVisibility(View.GONE);
            scheduledContinue.setVisibility(View.GONE);
        } else if (isTimeSelected) {
            pickupDelivery.setVisibility(View.GONE);
            llProceedToPayment.setVisibility(View.VISIBLE);
            scheduledContinue.setVisibility(View.GONE);
        } else {
            pickupDelivery.setVisibility(View.VISIBLE);
            llProceedToPayment.setVisibility(View.GONE);
            scheduledContinue.setVisibility(View.GONE);
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (toolbar != null) {
            toolbar.removeView(toolbarLayout);
        }
    }

    public void FeedbackDialog() {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.feedback);
        EditText commentEdit = dialog.findViewById(R.id.comment);

        Button submitBtn = dialog.findViewById(R.id.submit);
        submitBtn.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
    }

    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        System.out.println("CartFragment");
        toolbar = (ViewGroup) getActivity().findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setVisibility(View.VISIBLE);
            toolbarLayout = LayoutInflater.from(context).inflate(R.layout.toolbar_cart, toolbar, false);
            toolbarLayout.setBackgroundColor(getResources().getColor(R.color.theme));
            toolbar.addView(toolbarLayout);
        }
    }

    @OnClick({R.id.add_address_txt, R.id.btnPickup, R.id.btnDelivery, R.id.imgTimeClose,
            R.id.btnProceedToPayment, R.id.btnSchedule, R.id.btnContinue, R.id.tvChangeDate,
            R.id.imgAddressClose})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_address_txt:
            case R.id.btnDelivery:
                /**  If address is empty */
                startActivityForResult(new Intent(getActivity(), SetDeliveryLocationActivity.class).putExtra("get_address", true), ADDRESS_SELECTION);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
              /*  if (addAddressTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.change_address))) {
                    startActivityForResult(new Intent(getActivity(), SetDeliveryLocationActivity.class)
                            .putExtra("get_address", true), ADDRESS_SELECTION);
                    getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                }
                *//**  If address is filled *//*
                else if (addAddressTxt.getText().toString().equalsIgnoreCase(getResources().getString(R.string.add_address))) {
                    startActivityForResult(new Intent(getActivity(), SaveDeliveryLocationActivity.class).putExtra("get_address", true), ADDRESS_SELECTION);
                    getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);
                }*/
                break;
            case R.id.btnPickup:
                /**  If address is filled */
                startActivity(new Intent(context, AccountPaymentActivity.class).putExtra("is_show_wallet", true).putExtra("is_show_cash", true));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.anim_nothing);

                break;

            case R.id.btnContinue:
                Calendar currentCalendar = Calendar.getInstance();
                Format f = new SimpleDateFormat("HH:mm:ss");
                schedule_time = f.format(currentCalendar.getTime());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                schedule_date = sdf.format(currentCalendar.getTime());
                addressDeliveryTime.setText(Utils.getDayAndTimeFormat(currentCalendar.getTime()));
                isTimeSelected = true;
                setFLowVisibility();
                break;
            case R.id.btnSchedule:
            case R.id.tvChangeDate:
                showDialog();
                break;

            case R.id.imgAddressClose:
                isAddressSelected = false;
                addressDetail.setText("");
                locationInfoLayout.setVisibility(View.GONE);
                setFLowVisibility();
                break;
            case R.id.imgTimeClose:
                isTimeSelected = false;
                addressDeliveryTime.setText("");
                rlTimeLayout.setVisibility(View.GONE);
                schedule_date = "";
                schedule_time = "";
                setFLowVisibility();
                break;

            case R.id.btnProceedToPayment:
                if (connectionHelper.isConnectingToInternet()) {
//                    checkOut(GlobalData.getInstance().selectedAddress.getId());
                    checkoutMap = new HashMap<>();
                    checkoutMap.put("user_address_id", "" + GlobalData.getInstance().selectedAddress.getId());
                    checkoutMap.put("note", "" + customNotes.getText());
                    checkoutMap.put("delivery_date", schedule_date + " " + schedule_time);
                    checkoutMap.put("order_type", /*schedule_date + " " + schedule_time*/"DELIVERY");

                    if (from_.equalsIgnoreCase("id"))
                        checkoutMap.put("promocode_id", promo_code);
                    else
                        checkoutMap.put("couponcode", promo_code);

                    if (useWalletChkBox.isChecked())
                        checkoutMap.put("wallet", "1");
                    else
                        checkoutMap.put("wallet", "0");

                    checkoutMap.put("tips", String.format(Locale.getDefault(), "%.2f", tipForProvider));
                    Log.d("checkoutMap",checkoutMap.toString());
                    startActivity(new Intent(context, AccountPaymentActivity.class).putExtra("is_show_wallet", false).putExtra("is_show_cash", true));
                    activity.overridePendingTransition(R.anim.anim_nothing, R.anim.slide_out_right);
                } else {
                    Utils.displayMessage(activity, context, getString(R.string.oops_connect_your_internet));
                }
                break;

        }
    }

    private void showDialog() {
        androidx.appcompat.app.AlertDialog dialogBuilder = new androidx.appcompat.app.AlertDialog.Builder(getActivity()).create();
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_schedule_date, null);
        DatePicker datePicker = dialogView.findViewById(R.id.datePicker);
        TimePicker timePicker = dialogView.findViewById(R.id.timePicker);
        dialogView.findViewById(R.id.close_img).setOnClickListener(view -> dialogBuilder.cancel());

        dialogView.findViewById(R.id.btnDone).setOnClickListener(view -> {
            myCalendar.set(Calendar.YEAR, datePicker.getYear());
            myCalendar.set(Calendar.MONTH, datePicker.getMonth());
            myCalendar.set(Calendar.DAY_OF_MONTH, datePicker.getDayOfMonth());
            myCalendar.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
            myCalendar.set(Calendar.MINUTE, timePicker.getCurrentMinute());
            Calendar currentTimeTwoHours = Calendar.getInstance();
            currentTimeTwoHours.add(Calendar.MINUTE, -1);
            if (myCalendar.getTimeInMillis() < currentTimeTwoHours.getTimeInMillis()) {
                Utils.displayMessage(activity, context, getString(R.string.invalid_date_time));
                return;
            }
            Format f = new SimpleDateFormat("HH:mm:ss");
            schedule_time = f.format(myCalendar.getTime());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            schedule_date = sdf.format(myCalendar.getTime());
            addressDeliveryTime.setText(Utils.getDayAndTimeFormat(myCalendar.getTime()));
            isTimeSelected = true;
            setFLowVisibility();
            dialogBuilder.cancel();
        });
        dialogBuilder.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialogBuilder.setView(dialogView);
        dialogBuilder.show();
        /*final android.support.v7.app.AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.setCancelable(false);
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        alertDialog.show();*/

    }

    private void getViewCartWithPromocode(String promotion) {
        Call<AddCart> call = apiInterface.getViewCartPromocode(promotion);
        call.enqueue(new Callback<AddCart>() {
            @Override
            public void onResponse(Call<AddCart> call, Response<AddCart> response) {
                skeleton.hide();
                if (!response.isSuccessful() && response.errorBody() != null) {
                    errorLayout.setVisibility(View.VISIBLE);
                    dataLayout.setVisibility(View.GONE);
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
//                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    customDialog.dismiss();
                    //get Item Count
                    itemCount = response.body().getProductList().size();
                    GlobalData.notificationCount = response.body().getProductList().size();
                    if (itemCount == 0) {
                        errorLayout.setVisibility(View.VISIBLE);
                        dataLayout.setVisibility(View.GONE);
                        GlobalData.addCart = response.body();
                        GlobalData.addCart = null;
                    } else {
                        AddCart addCart = response.body();
                        GlobalData.cartItemTotal = response.body().getPayable();
                        viewCartItemList.clear();
                        viewCartItemList = addCart.getProductList();
                        viewCartAdapter = new ViewCartAdapter(viewCartItemList, activity);
                        orderItemRv.setAdapter(viewCartAdapter);
                        viewCartAdapter.notifyDataSetChanged();

                        errorLayout.setVisibility(View.GONE);
                        dataLayout.setVisibility(View.VISIBLE);
                        GlobalData.notificationCount = itemQuantity;
                        GlobalData.addCartShopId = response.body().getProductList().get(0).getProduct().getShopId();
                        //Set Payment details
                        String currency = response.body().getProductList().get(0).getProduct().getPrices().getCurrency();

                        if (response.body().getPromocodeAmount() > 0) {
                            lnrPromocodeAmount.setVisibility(View.VISIBLE);
                            promoCodeApply.setText("Promocode Applied.");
                            promoCodeApply.setEnabled(false);
                        } else {
                            promoCodeApply.setText("Apply");
                            promoCodeApply.setEnabled(true);
                            lnrPromocodeAmount.setVisibility(View.GONE);
                        }

                        itemTotalAmount.setText(String.format(Locale.getDefault(), "%s%.2f", currency, response.body().getTotalPrice()));
                        discountAmount.setText(String.format(Locale.getDefault(), "- %s%.2f", currency, response.body().getShopDiscount()));
                        promoCodeApply.setText(currency + response.body().getPromocodeAmount());
                        serviceTax.setText(currency + response.body().getTax());
                        payAmount.setText(String.format(Locale.getDefault(), "%s%s", currency, response.body().getPayable()));
                        deliveryCharges.setText(String.format("%s%s", response.body().getProductList().get(0).getProduct().getPrices().getCurrency(), response.body().getDeliveryCharges()));
                        //Set Restaurant Details
                        restaurantName.setText(response.body().getProductList().get(0).getProduct().getShop().getName());
                        restaurantDescription.setText(response.body().getProductList().get(0).getProduct().getShop().getDescription());
                        String image_url = response.body().getProductList().get(0).getProduct().getShop().getAvatar();
                        Glide.with(context)
                                .load(image_url)
                                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_restaurant_place_holder).error(R.drawable.ic_restaurant_place_holder))
                                .into(restaurantImage);

                    }
                }
            }

            @Override
            public void onFailure(Call<AddCart> call, Throwable t) {
                errorLayout.setVisibility(View.VISIBLE);
                dataLayout.setVisibility(View.GONE);
            }
        });
    }

    private void getViewCartWithPromocodeCODE(String promotion) {
        Call<AddCart> call = apiInterface.getViewCartPromocodeCODE(promotion);
        call.enqueue(new Callback<AddCart>() {
            @Override
            public void onResponse(Call<AddCart> call, Response<AddCart> response) {
                skeleton.hide();
                if (!response.isSuccessful() && response.errorBody() != null) {
                    errorLayout.setVisibility(View.VISIBLE);
                    dataLayout.setVisibility(View.GONE);
                    try {
                        JSONObject jObjError = new JSONObject(response.errorBody().string());
                        Toast.makeText(context, jObjError.optString("message"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
//                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } else if (response.isSuccessful()) {
                    customDialog.dismiss();
                    //get Item Count
                    itemCount = response.body().getProductList().size();
                    GlobalData.notificationCount = response.body().getProductList().size();
                    GlobalData.cartItemTotal = response.body().getPayable();
                    if (itemCount == 0) {
                        errorLayout.setVisibility(View.VISIBLE);
                        dataLayout.setVisibility(View.GONE);
                        GlobalData.addCart = response.body();
                        GlobalData.addCart = null;
                    } else {
                        AddCart addCart = response.body();

                        viewCartItemList.clear();
                        viewCartItemList = addCart.getProductList();
                        viewCartAdapter = new ViewCartAdapter(viewCartItemList, activity);
                        orderItemRv.setAdapter(viewCartAdapter);
                        viewCartAdapter.notifyDataSetChanged();

                        errorLayout.setVisibility(View.GONE);
                        dataLayout.setVisibility(View.VISIBLE);
                        GlobalData.notificationCount = itemQuantity;
                        GlobalData.addCartShopId = response.body().getProductList().get(0).getProduct().getShopId();
                        //Set Payment details
                        String currency = response.body().getProductList().get(0).getProduct().getPrices().getCurrency();

                        if (response.body().getPromocodeAmount() > 0) {
                            lnrPromocodeAmount.setVisibility(View.VISIBLE);
                            promoCodeApply.setText("Promocode Applied.");
                            removePromoCode.setVisibility(View.VISIBLE);
                            promoCodeApply.setEnabled(false);
                        } else {
                            promoCodeApply.setText("Apply");
                            promoCodeApply.setEnabled(true);
                            removePromoCode.setVisibility(View.GONE);
                            lnrPromocodeAmount.setVisibility(View.GONE);
                        }

                        itemTotalAmount.setText(String.format(Locale.getDefault(), "%s%.2f", currency, response.body().getTotalPrice()));
                        discountAmount.setText(String.format(Locale.getDefault(), "- %s%.2f", currency, response.body().getShopDiscount()));
                        promoCodeApply.setText(currency + response.body().getPromocodeAmount());
                        serviceTax.setText(currency + response.body().getTax());
                        payAmount.setText(String.format(Locale.getDefault(), "%s%s", currency, response.body().getPayable()));
                        deliveryCharges.setText(String.format("%s%s", response.body().getProductList().get(0).getProduct().getPrices().getCurrency(), response.body().getDeliveryCharges()));
                        //Set Restaurant Details
                        restaurantName.setText(response.body().getProductList().get(0).getProduct().getShop().getName());
                        restaurantDescription.setText(response.body().getProductList().get(0).getProduct().getShop().getDescription());
                        String image_url = response.body().getProductList().get(0).getProduct().getShop().getAvatar();
                        Glide.with(context)
                                .load(image_url)
                                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).placeholder(R.drawable.ic_restaurant_place_holder).error(R.drawable.ic_restaurant_place_holder))
                                .into(restaurantImage);
                    }
                }
            }

            @Override
            public void onFailure(Call<AddCart> call, Throwable t) {
                errorLayout.setVisibility(View.VISIBLE);
                dataLayout.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.print("CartFragment");
        if (requestCode == ADDRESS_SELECTION && resultCode == Activity.RESULT_OK) {
            isAddressSelected = true;
            System.out.print("CartFragment : Success");
            if (GlobalData.getInstance().selectedAddress != null) {
                locationErrorLayout.setVisibility(View.GONE);
                locationInfoLayout.setVisibility(View.VISIBLE);
                //Intialize address Value
                if (GlobalData.getInstance().selectedAddress != null && GlobalData.getInstance().selectedAddress.getLandmark() != null) {
                    if (GlobalData.getInstance().addressList.getAddresses().size() == 1)
                        addAddressTxt.setText(getString(R.string.add_address));
                    else
                        addAddressTxt.setText(getString(R.string.change_address));
                }
                addressDetail.setText(GlobalData.getInstance().selectedAddress.getMapAddress());
                //addressDeliveryTime.setText(viewCartItemList.get(0).getProduct().getShop().getEstimatedDeliveryTime() + " Mins");
            } else {
                locationErrorLayout.setVisibility(View.VISIBLE);
                locationInfoLayout.setVisibility(View.GONE);
            }
        } else if (requestCode == ADDRESS_SELECTION && resultCode == Activity.RESULT_CANCELED) {
            System.out.print("CartFragment : Failure");

        } else if (requestCode == PROMOCODE_APPLY) {
            if (data != null) {
                promoCodeApply.setText(R.string.promo_apply_text);
                promoCodeApply.setEnabled(false);
                promo_code = data.getExtras().getString("promotion");
                from_ = data.getExtras().getString("from");
                if (from_.equalsIgnoreCase("id")) {
                    params.put("promocode_id", promo_code);
                    getViewCartWithPromocode(promo_code);
                } else {
                    params.put("couponcode", promo_code);
                    getViewCartWithPromocodeCODE(promo_code);
                }
            }
        }
    }

    @OnClick(R.id.wallet_layout)
    public void onViewClicked() {
    }

    @OnClick(R.id.custom_notes)
    public void onAddCustomNotesClicked() {
        try {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

            final FrameLayout frameView = new FrameLayout(getActivity());
            builder.setView(frameView);

            final AlertDialog alertDialog = builder.create();
            LayoutInflater inflater = alertDialog.getLayoutInflater();
            View dialogView = inflater.inflate(R.layout.custom_note_popup, frameView);

            final EditText notes = dialogView.findViewById(R.id.notes);
            notes.setText(customNotes.getText());
            Button submit = (Button) dialogView.findViewById(R.id.custom_note_submit);
            submit.setOnClickListener(v -> {
                customNotes.setText(notes.getText());
                alertDialog.dismiss();
            });
            alertDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}